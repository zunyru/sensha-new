<header id="header">
    <div class="header-wrap">
        <div class="header-main">
            <div class="inner">
                <a class="header-logo" href="<?php echo base_url("$this->coutry_iso"); ?>">
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/logo.png" alt="SENSHA">
                </a>
                <ul class="header-menu">
                    <?php if (!$this->ion_auth->logged_in()) : ?>
                        <!--
            <li>
              <a href="<?php echo base_url("$coutry_iso" . "page/register"); ?>" class="">
                <span class="icon-person"></span>
                <?php echo $this->lang->line('header_menu_sign_up', FALSE); ?>
              </a>
            </li>
-->
                        <li>
                            <a href="" data-src="#modal01" class="login">
                                <span class="icon-lock"></span>
                                <?php echo $this->lang->line('header_menu_login', FALSE); ?>
                            </a>
                        </li>
                    <?php else : ?>
                        <li>
                            <a href="<?php echo base_url("/page/user/dashboard"); ?>" class="">
                                <span class="icon-person"></span>
                                <?php echo $this->ion_auth->user()->row()->email; ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("/page/user/logout"); ?>" class="">
                                <span class="icon-lock"></span>
                                <?php echo $this->lang->line('header_menu_logout', FALSE); ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li class="">
                        <a href="<?php echo base_url("$coutry_iso" . "page/cart"); ?>" class="">
                            <span class="icon-cart02"></span>
                            <?php echo $this->lang->line('header_menu_cart', FALSE); ?>
                        </a>
                    </li>
                    <li class="lang">
                        <a href="" class="">
                            <span class="icon-lang"></span>
                            <?php echo ucwords(str_replace("_", " ", $this->user_lang)) ?>
                        </a>
                        <div class="language-select">
                            <?php
                            $user_country = $this->ion_auth->user()->row()->country;
                            if ($this->ion_auth->logged_in()) {
                                $this->db->where('country', $user_country);
                                $this->db->where('is_active', 1); // is_active が 1 の条件を追加
                                $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                                if (empty($nation_lang)) {
                                    $this->db->where('country', 'Global'); // Global を指定
                                    $this->db->where('is_active', 1); // is_active が 1 の条件を追加
                                    $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                                }
                            } else {
                                $this->db->where('is_active', 1); // is_active が 1 の条件を追加
                                $this->db->where_not_in('country', ['JapanA', 'JapanB']); // JapanA と JapanB を除外
                                $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                            }
                            ?>
                            <ul>
                                <?php foreach ($nation_lang as $item) : ?>
                                    <?php
                                        $this->db->where('nicename', $item->country);
                                        $c = $this->datacontrol_model->getRowData('countries');
                                        //echo $this->db->last_query();
                                        $CI =& get_instance();//for getting instance
                                        $slug_name = $CI->uri->segment(1);
                                        $seleted = false;
    
                                        if ($slug_name == strtolower($c->iso)) {
                                            $seleted = true;
                                        } elseif (strlen($slug_name) > 2) {
                                            if ($item->country == 'Global') {
                                                $seleted = true;
                                            }
                                        }
                                    ?>
                                        <li>
                                            <label
                                                    for="language<?php echo $item->country ?>">
                                                <input
                                                        type="radio"
                                                        name="language"
                                                        data-iso="<?php echo strtolower($c->iso); ?>"
                                                        value="<?php echo $item->country; ?>"
                                                        data-label="language-select"
                                                        id="language<?php echo $item->country ?>"
                                                        data-value="<?php echo $item->country; ?>"
                                                    <?php echo ($user_country == $item->country) ? 'checked' : ''; ?>
                                                    <?php echo ($seleted) ? 'checked' : ''; ?>
                                                >
                                                <?php echo ucwords(str_replace("_", " ", $item->country)) ?>
                                            </label>
                                        </li>
                                    <?php endforeach; ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- .header-main -->
        <div id="hamburger">
            <div id="hamburger-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
        </div>
        <?php
        $country = 'Global';
        if ($this->ion_auth->logged_in()) {
            $country = $this->ion_auth->user()->row()->country;
        } else {
            $country = $this->user_lang;
        }
        if ($country == 'JapanA' || $country == 'JapanB') {
            $country = 'Japan';
        }

        //zun
        $nation_lang = $this->datacontrol_model->getRowData('nation_lang', array('country' => $country));

        if (!empty($nation_lang)) {
            $this->db->where('country', $country);
        } else {
            $this->db->where('country', 'Global');
        }

        $this->db->where('cluster_type', 'general');
        //$this->db->order_by('cluster_name', 'ASC');
        $query = $this->db->get('cluster1');
        $cluster1 = $query->result();

        //echo $this->db->last_query();
        ?>
        <div class="header-nav">
            <div class="inner">
                <nav>
                    <ul>
                        <?php if ($this->ion_auth->logged_in()) : ?>
                            <li class="welcome">
                                <a href="<?php echo base_url("/page/user/dashboard"); ?>" class="">
                                    Hello
                                    <span class="icon-person"></span>
                                    <?php echo $this->ion_auth->user()->row()->email; ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php foreach ($cluster1 as $item) : ?>
                            <?php if ((!$this->ion_auth->logged_in() && $item->cluster_url == 'Sample') || ($this->ion_auth->in_group(array('GU')) && $item->cluster_url == 'Sample')) : ?>

                            <?php else : ?>
                                <li>
                                    <a href="<?php echo base_url("$coutry_iso" . "page/general/$item->cluster_url"); ?>"><?php echo $item->cluster_name; ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <?php
                $country = 'Global';
                if ($this->ion_auth->logged_in()) {
                    $country = $this->ion_auth->user()->row()->country;
                    $exclude_group_id = $this->ion_auth->user()->row()->exclude_group;
                } else {
                    $country = $this->user_lang;
                }

                if ($exclude_group_id != '') {
                    $this->db->where('id', $exclude_group_id);
                    $query = $this->db->get('exclude_group');
                    $result = $query->row();

                    $this->db->where_not_in('id', explode(',', $result->category));
                }

                $nation_lang = $this->datacontrol_model->getRowData('nation_lang', array('country' => $country));
                if (!empty($nation_lang)) {
                    $this->db->where('country', $country);
                } else {
                    $this->db->where('country', 'Global');
                }

                $this->db->where('cluster_type', 'ppf');
                $this->db->order_by('cluster_name', 'ASC');
                $query = $this->db->get('cluster1');
                $cluster1 = $query->result();

                if ($exclude_group_id != '') {
                    $this->db->where('id', $exclude_group_id);
                    $query = $this->db->get('exclude_group');
                    $result = $query->row();

                    $this->db->where_not_in('level1', explode(',', $result->category));
                }
                if (!empty($nation_lang)) {
                    $this->db->where('country', $country);
                } else {
                    $this->db->where('country', 'Global');
                }
                $this->db->select('id,country,cluster_type,cluster_name,level1,cluster_url');
                $this->db->where('cluster_type', 'ppf');
                $this->db->order_by('cluster_name', 'asc');
                $query = $this->db->get('cluster2');
                $cluster2 = $query->result();
                //echo $this->db->last_query();


                ?>
                <!-- Mobile -->
                <form id="ppf_form_mobile" method="post" action="<?php echo base_url("page/ppf"); ?>">
                    <div class="select-list-block sp_view">
                        <p class="ttl">PPF Search</p>
                        <ul class="select-list">
                            <li class="maker">
                                <span class="icon-maker"></span>
                                <select name="cat_1" required>
                                    <option data-url="" value="Select Maker">Maker</option>
                                    <?php foreach ($cluster1 as $item) : ?>
                                        <option data-url="<?php echo $item->cluster_url; ?>"
                                                value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_1') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li class="model">
                                <span class="icon-model"></span>
                                <select name="cat_2">
                                    <option data-url="" value="">Select Model</option>
                                    <?php foreach ($cluster2 as $item) : ?>
                                        <?php if (empty($this->input->post('cat_1')) || $this->input->post('cat_1') == $item->level1) { ?>
                                            <option data-level1="<?php echo $item->level1; ?>"
                                                    data-url="<?php echo $item->cluster_url; ?>"
                                                    value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_2') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                        <?php } else { ?>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li class="part">
                                <span class="icon-part"></span>
                                <select name="parts">
                                    <option value="" <?php if ($_REQUEST['part'] == '') {
                                        echo 'selected';
                                    } ?>>Select Part
                                    </option>
                                    <option value="headlight" <?php if ($_REQUEST['part'] == 'headlight') {
                                        echo 'selected';
                                    } ?>>Headlight
                                    </option>
                                    <option value="f-bumper" <?php if ($_REQUEST['part'] == 'f-bumper') {
                                        echo 'selected';
                                    } ?>>F-bumper
                                    </option>
                                    <option value="r-bumper" <?php if ($_REQUEST['part'] == 'r-bumper') {
                                        echo 'selected';
                                    } ?>>R-bumper
                                    </option>
                                    <option value="bumper-set" <?php if ($_REQUEST['part'] == 'bumper-set') {
                                        echo 'selected';
                                    } ?>>Bumper set
                                    </option>
                                    <option value="luggage" <?php if ($_REQUEST['part'] == 'luggage') {
                                        echo 'selected';
                                    } ?>>Luggage
                                    </option>
                                    <option value="doorcup" <?php if ($_REQUEST['part'] == 'doorcup') {
                                        echo 'selected';
                                    } ?>>Doorcup
                                    </option>
                                    <option value="side-spoiler" <?php if ($_REQUEST['part'] == 'side-spoiler') {
                                        echo 'selected';
                                    } ?>>Side spoiler
                                    </option>
                                    <option value="aluminium-mold" <?php if ($_REQUEST['part'] == 'aluminium-mold') {
                                        echo 'selected';
                                    } ?>>Aluminium Mold
                                    </option>
                                    <option value="pillar-garnish" <?php if ($_REQUEST['part'] == 'pillar-garnish') {
                                        echo 'selected';
                                    } ?>>Pillar Garnish
                                    </option>
                                </select>
                            </li>
                        </ul>
                        <div class="search-btn">
                            <button type="submit">
                                <span>SEARCH</span>
                                <span class="icon-search02 ico-glass"></span>
                            </button>
                        </div>
                    </div>
                </form>


                <form id="ppf_form_desktop" method="get" action="#">
                    <input type="hidden" name="ppf" value="1"/>
                    <div class="header-search">
                        <a data-src=""
                           class="search-button"><?php echo $this->lang->line('navi_cut_film_title', FALSE); ?>
                            <span class="icon-search02 ico-glass"></span>
                        </a>
                        <div class="header-search-pop">
                            <div class="search-content">
                                <p class="ttl"><?php echo $this->lang->line('navi_cut_film_title_search', FALSE); ?></p>
                                <p class="lead"><?php echo $this->lang->line('navi_cut_film_easy_to', FALSE); ?></p>
                                <ul class="search-list">
                                    <li class="maker">
                                        <span class="icon-maker"><?php echo $this->lang->line('navi_cut_film_select_maker', FALSE); ?></span>
                                        <select name="cat_1" required>
                                            <option value="">Select Maker</option>
                                            <?php foreach ($cluster1 as $item) : ?>
                                                <option data-url="<?php echo $item->cluster_url; ?>"
                                                        value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_1') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </li>
                                    <li class="model">
                                        <span class="icon-model"><?php echo $this->lang->line('navi_cut_film_select_model', FALSE); ?></span>
                                        <select name="cat_2">
                                            <option value="">Select Model</option>
                                            <?php foreach ($cluster2 as $item) : ?>
                                                <?php if (empty($this->input->post('cat_1')) || $this->input->post('cat_1') == $item->level1) { ?>
                                                    <option data-level1="<?php echo $item->level1; ?>"
                                                            data-url="<?php echo $item->cluster_url; ?>"
                                                            value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_2') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                                <?php } else { ?>
                                                <?php } ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </li>
                                    <li class="part">
                                        <span class="icon-part"><?php echo $this->lang->line('navi_cut_film_select_part', FALSE); ?></span>
                                        <select name="parts">
                                            <option value="" <?php if ($_REQUEST['parts'] == '') {
                                                echo 'selected';
                                            } ?>>Select Part
                                            </option>
                                            <option value="headlight" <?php if ($_REQUEST['parts'] == 'headlight') {
                                                echo 'selected';
                                            } ?>>Headlight
                                            </option>
                                            <option value="f-bumper" <?php if ($_REQUEST['parts'] == 'f-bumper') {
                                                echo 'selected';
                                            } ?>>F-bumper
                                            </option>
                                            <option value="r-bumper" <?php if ($_REQUEST['parts'] == 'r-bumper') {
                                                echo 'selected';
                                            } ?>>R-bumper
                                            </option>
                                            <option value="bumper-set" <?php if ($_REQUEST['parts'] == 'bumper-set') {
                                                echo 'selected';
                                            } ?>>Bumper set
                                            </option>
                                            <option value="luggage" <?php if ($_REQUEST['parts'] == 'luggage') {
                                                echo 'selected';
                                            } ?>>Luggage
                                            </option>
                                            <option value="doorcup" <?php if ($_REQUEST['parts'] == 'doorcup') {
                                                echo 'selected';
                                            } ?>>Doorcup
                                            </option>
                                            <option value="side-spoiler" <?php if ($_REQUEST['parts'] == 'side-spoiler') {
                                                echo 'selected';
                                            } ?>>Side spoiler
                                            </option>
                                            <option value="aluminium-mold" <?php if ($_REQUEST['parts'] == 'aluminium-mold') {
                                                echo 'selected';
                                            } ?>>Aluminium Mold
                                            </option>
                                            <option value="pillar-garnish" <?php if ($_REQUEST['part'] == 'pillar-garnish') {
                                                echo 'selected';
                                            } ?>>Pillar Garnish
                                            </option>
                                        </select>
                                    </li>
                                </ul>
                                <div class="serch-btn">
                                    <button type="submit" id="sbtn2">
                                        <span><?php echo $this->lang->line('navi_cut_film_btn_search', FALSE); ?></span>
                                        <span class="icon-search02 ico-glass"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="clear: both"></div>
                <div class="lang mobile">
                    <div class="current_lang">
                        <span>Select Language</span>
                        <span class="icon-lang" id="lang-btn-sp"></span>
                        <?php //echo (get_cookie('user_language') == '') ? "Global" : get_cookie('user_language'); ?>
                        <?php echo ucwords(str_replace("_", " ", $this->user_lang)) ?>
                    </div>
                    <div class="language-select">
                            <?php
                    $user_country = $this->ion_auth->user()->row()->country;
                    if ($this->ion_auth->logged_in()) {
                        $this->db->where('country', $user_country);
                        $this->db->where('is_active', 1); // is_active が 1 の条件を追加
                        $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                        if (empty($nation_lang)) {
                            $this->db->where('country', 'Global'); // Global を指定
                            $this->db->where('is_active', 1); // is_active が 1 の条件を追加
                            $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                        }
                    } else {
                        $this->db->where('is_active', 1); // is_active が 1 の条件を追加
                        $this->db->where_not_in('country', ['JapanA', 'JapanB']); // JapanA と JapanB を除外
                        $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                    }
                    ?>
                        <ul id="lang-content-sp">
                            <?php foreach ($nation_lang as $item) : ?>
                                <?php
                                        $this->db->where('nicename', $item->country);
                                $c = $this->datacontrol_model->getRowData('countries');
                                //echo $this->db->last_query();
                                $CI =& get_instance();//for getting instance
                                $slug_name = $CI->uri->segment(1);
                                $seleted = false;

                                if ($slug_name == strtolower($c->iso)) {
                                    $seleted = true;
                                } elseif (strlen($slug_name) > 2) {
                                    if ($item->country == 'Global') {
                                        $seleted = true;
                                    }
                                }
                                    ?>
                                    <li>
                                        <label
                                                for="sp-language<?php echo $item->country; ?>">
                                            <input
                                                    type="radio"
                                                    name="language"
                                                    data-iso="<?php echo strtolower($c->iso); ?>"
                                                    value="<?php echo $item->country; ?>"
                                                    data-label="language-select"
                                                    id="sp-language<?php echo $item->country; ?>"
                                                    data-value="<?php echo $item->country; ?>"

                                                <?php echo ($user_country == $item->country) ? 'checked' : ''; ?>
                                                <?php echo ($seleted) ? 'checked' : ''; ?>
                                            >
                                            <?php echo ucwords(str_replace("_", " ", $item->country)) ?></label>
                                    </li>
                                <?php endforeach; ?>
                        </ul>


                    </div>
                </div>
            </div>
        </div>
        <?php if (!$this->ion_auth->logged_in()) : ?>
            <div class="sp-login">
                <a href="" data-src="#modal01" class="sp-login-btn">
                    <span class="icon-person"></span>
                    Login
                </a>
            </div>
        <?php else : ?>
            <div class="sp-login">
                <a href="<?php echo base_url("/page/user/logout"); ?>">
                    <span class="icon-person"></span>
                    Logout
                </a>
            </div>
        <?php endif; ?>
    </div>
</header>
<script>
    var cat1 = "";
    var cat2 = "";
    var cat3 = "";
    var ppf_cluster1 = JSON.parse('<?php echo json_encode($cluster1); ?>');
    var ppf_cluster2 = JSON.parse('<?php echo json_encode($cluster2); ?>');

    $("#lang-btn-sp").click(function (event) {
        $("#lang-content-sp").slideToggle();
        $("html,body").animate({
            scrollTop: $('#hamburger').offset().top
        }, 1000);
    });

    function titleCase(str) {
        return str.toLowerCase().replace(/\b(\w)/g, s => s.toUpperCase());
    }

    $(function () {
        $('#ppf_form_desktop select[name="cat_1"]').change(function () {
            $('#ppf_form_desktop select[name="cat_2"]').html('');
            $('#ppf_form_desktop select[name="parts"]').html('<option data-url="" value="">Select Part</option>');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_desktop select[name="cat_2"]').append('<option data-url="' + ppf_cluster2[i].cluster_url + '" value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
            var option_none = '<option value="">Select Model</option>';
            $('#ppf_form_desktop select[name="cat_2"]').prepend(option_none);
            $('#ppf_form_desktop select[name="cat_2"]').prop("selectedIndex", 0);

            //cat1 = $('#ppf_form_desktop select[name="cat_1"] option:selected').text();
            cat1_id = $('#ppf_form_desktop select[name="cat_1"] option:selected').val();
            cat1 = $('#ppf_form_desktop select[name="cat_1"] option:selected').attr('data-url');
        });

        $('#ppf_form_desktop select[name="cat_2"]').change(function () {
            //cat2 = $('#ppf_form_desktop select[name="cat_2"] option:selected').text();
            cat2_id = $('#ppf_form_desktop select[name="cat_2"] option:selected').val();
            cat2 = $('#ppf_form_desktop select[name="cat_2"] option:selected').attr('data-url');
            console.log(cat2);
            $('#ppf_form_desktop select[name="parts"]').html('');

            $.ajax({
                method: "POST",
                url: '<?php echo base_url("page/product/get_parts") ?>',
                data: {cat1: cat1_id, cat2: cat2_id},
                success: function (result) {
                    var option_none = '<option value="">Select Part</option>';
                    var response = JSON.parse(result);
                    $('#ppf_form_desktop select[name="parts"]').prepend(option_none);
                    for (i = 0; i < response.length; i++) {
                        $('#ppf_form_desktop select[name="parts"]').append('<option value="' + response[i].parts_key + '">'
                            + response[i].parts + '</option>');
                    }
                }
            });
        });

        $('#ppf_form_desktop select[name="parts"]').change(function () {

            cat3 = $('#ppf_form_desktop select[name="parts"] option:selected').val();
            //cat3 = cat3.replace(" ", "-");
            //cat3 = cat3.replace("/", " ");
        });

        $(document).ready(function () {
            $("#ppf_form_desktop").submit(function () {
                if (cat1 != 'Select-Maker') {
                    str = cat1;

                    if (cat2 != 'Select-Model') {
                        str = cat1 + '/' + cat2;
                    }

                    if (cat3 != 'Select-Part') {
                        str = cat1 + '/' + cat2 + "/" + cat3;
                    }
                }
                str = str.replace("//", "/");
                str_url = '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>' + str.toLowerCase();
                //console.log(str_url)
                window.location.replace(str_url);
                return false;
            });
        });

        $('#ppf_form_mobile select[name="cat_1"]').change(function () {

            $('#ppf_form_mobile select[name="cat_2"]').html('');
            $('#ppf_form_mobile select[name="parts"]').html('<option value="">Select Part</option>');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_mobile select[name="cat_2"]').append('<option data-url="' + ppf_cluster2[i].cluster_url + '" value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
            var option_none = '<option data-url="" value="">Select Model</option>';
            $('#ppf_form_mobile select[name="cat_2"]').prepend(option_none);
            $('#ppf_form_mobile select[name="cat_2"]').prop("selectedIndex", 0);

            //cat1 = $('#ppf_form_mobile select[name="cat_1"] option:selected').text();
            cat1 = $('#ppf_form_mobile select[name="cat_1"] option:selected').attr('data-url');
            cat1_id = $('#ppf_form_mobile select[name="cat_1"] option:selected').val();

        });

        $('#ppf_form_mobile select[name="cat_2"]').change(function () {
            //cat2 = $('#ppf_form_mobile select[name="cat_2"] option:selected').text();
            cat2 = $('#ppf_form_mobile select[name="cat_2"] option:selected').attr('data-url');
            cat2_id = $('#ppf_form_mobile select[name="cat_2"] option:selected').val();
            $('#ppf_form_mobile select[name="parts"]').html('');

            $.ajax({
                method: "POST",
                url: '<?php echo base_url("page/product/get_parts") ?>',
                data: {cat1: cat1_id, cat2: cat2_id},
                success: function (result) {
                    var option_none = '<option value="">Select Part</option>';
                    var response = JSON.parse(result);
                    $('#ppf_form_mobile select[name="parts"]').prepend(option_none);
                    for (i = 0; i < response.length; i++) {
                        $('#ppf_form_mobile select[name="parts"]').append('<option value="' + response[i].parts_key + '">'
                            + response[i].parts + '</option>');
                    }
                }
            });
        });

        $('#ppf_form_mobile select[name="parts"]').change(function () {
            cat3 = $('#ppf_form_mobile select[name="parts"] option:selected').val();
            //cat3 = cat3.replace(" ", "-");
            //cat3 = cat3.replace("/", " ");
        });

        $(document).ready(function () {
            $("#ppf_form_mobile").submit(function () {
                if (cat1 != 'Select-Maker') {
                    str = cat1;

                    if (cat2 != 'Select-Model') {
                        str = cat1 + '/' + cat2;
                    }

                    if (cat3 != 'Select-Part') {
                        str = cat1 + '/' + cat2 + "/" + cat3;
                    }
                }
                str = str.replace("//", "");
                str_url = '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>' + str.toLowerCase();
                //console.log(str_url)
                window.location.replace(str_url);
                return false;
            });
        });


        $('#ppf_form_home select[name="cat_1"]').change(function () {
            $('#ppf_form_home select[name="cat_2"]').html('');
            $('#ppf_form_home select[name="parts"]').html('<option data-url="" value="">Select Part</option>');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_home select[name="cat_2"]').append('<option data-url="' + ppf_cluster2[i].cluster_url + '" value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
            var option_none = '<option value="">Select Model</option>';
            $('#ppf_form_home select[name="cat_2"]').prepend(option_none);
            $('#ppf_form_home select[name="cat_2"]').prop("selectedIndex", 0);

            //cat1 = $('#ppf_form_home select[name="cat_1"] option:selected').text();
            cat1 = $('#ppf_form_home select[name="cat_1"] option:selected').attr('data-url');
            cat1_id = $('#ppf_form_home select[name="cat_1"] option:selected').val();
        });

        $('#ppf_form_home select[name="cat_2"]').change(function () {
            cat2 = $('#ppf_form_home select[name="cat_2"] option:selected').attr('data-url');
            cat2_id = $('#ppf_form_home select[name="cat_2"] option:selected').val();
            $('#ppf_form_home select[name="parts"]').html('');

            $.ajax({
                method: "POST",
                url: '<?php echo base_url("page/product/get_parts") ?>',
                data: {cat1: cat1_id, cat2: cat2_id},
                success: function (result) {
                    var option_none = '<option value="">Select Part</option>';
                    var response = JSON.parse(result);
                    $('#ppf_form_home select[name="parts"]').prepend(option_none);
                    for (i = 0; i < response.length; i++) {
                        $('#ppf_form_home select[name="parts"]').append('<option value="' + response[i].parts_key + '">'
                            + response[i].parts + '</option>');
                    }
                }
            });

        });

        $('#ppf_form_home select[name="parts"]').change(function () {
            cat3 = $('#ppf_form_home select[name="parts"] option:selected').val()

        });
    });
    $(document).ready(function () {
        $("#ppf_form_home").submit(function () {
            if (cat1 != 'Select-Model') {
                str = cat1;

                if (cat2 != 'Select-Model') {
                    str = cat1 + '/' + cat2;
                }

                if (cat3 != 'Select-Model') {
                    str = cat1 + '/' + cat2 + "/" + cat3;
                }
            }
            str = str.replace("//", "/");
            str_url = '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>' + str.toLowerCase();
            //console.log(str_url);
            window.location.replace(str_url);
            return false;
        });
    });

    function submitFilter(cat1, cat2) {
        if (cat1 != "" && cat2 != "") {
            console.log("cat2")
        }
        $('#ppf_form_desktop').attr('action', '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>');


    }
</script>