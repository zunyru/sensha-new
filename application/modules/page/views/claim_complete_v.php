    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_complete_claim', FALSE); ?></span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="box-content">
			<div class="layout-contain">
		    	<div class="clr box_form">
					<div class="topic">
						<p class="title-page"><?php echo $this->lang->line('page_complete_claim_claim_sent', FALSE); ?></p>
					</div>
					<div class="clr box-success">
						<div class="left">
					    	<img src="<?php echo base_url("assets/sensha-theme/");?>images/img-claim.png">
						</div>
						<div class="right">
							<h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_complete_claim_your_claim', FALSE); ?></h2>
						    <p><?php echo $this->lang->line('page_complete_claim_we_check', FALSE); ?></p>
						</div>
					</div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script>
