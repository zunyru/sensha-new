<div class="clr inner">
    <div id="breadcrumbs" style="margin:15px 0;">
        <span><a href="<?php echo base_url("$coutry_iso"); ?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_about_ppf', FALSE); ?></span>
    </div>
</div>
<div class="top-banner b-ppf">
    <div class="caption-b">
        <p class="head-banner"><?php echo htmlspecialchars_decode($page_content->content_a); ?></p>
        <h2><?php echo htmlspecialchars_decode($page_content->content_b); ?></h2>
        <p><?php echo htmlspecialchars_decode($page_content->content_c); ?></p>
    </div>
    <div class="img-ppf">
        <ul>
            <li><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/ppf1.png"></li>
            <li><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/ppf2.png"></li>
            <li><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/ppf3.png"></li>
        </ul>
    </div>
</div>
<div class="box_about_ppf">
    <div class="clr wrap">
        <div class="topic">
            <p class="title-page"><?php echo $this->lang->line('page_about_ppf_section1_title', FALSE); ?></p>
        </div>
        <div class="inner-ppf">
            <p align="center"><?php echo $this->lang->line('page_about_ppf_section1_desc', FALSE); ?></p>
            <div class="img-ppt">
                <figure><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/protection.svg"></figure>
                <div class="detail-img-ppt">
                    <div class="inner-detail-img-ppt">
                        <div class="inner-img">
                            <div class="box-line">
                                <div class="line1"></div>
                                <div class="line-detail"><?php echo $this->lang->line('page_about_ppf_section1_imgtxt1', FALSE); ?></div>
                            </div>
                        </div><!--inner-img-->
                        <div class="inner-img1">
                            <div class="box-line">
                                <div class="line1"></div>
                                <div class="line-detail"><?php echo $this->lang->line('page_about_ppf_section1_imgtxt2', FALSE); ?></div>
                            </div>
                        </div><!--inner-img1-->
                        <div class="inner-img2">
                            <div class="box-line">
                                <div class="line1"></div>
                                <div class="line-detail"><?php echo $this->lang->line('page_about_ppf_section1_imgtxt3', FALSE); ?></div>
                            </div>
                        </div><!--inner-img2-->
                        <div class="inner-img3">
                            <div class="box-line">
                                <div class="line1"></div>
                                <div class="line-detail"><?php echo $this->lang->line('page_about_ppf_section1_imgtxt4', FALSE); ?></div>
                            </div>
                        </div><!--inner-img3-->
                        <div class="inner-img4">
                            <div class="box-line">
                                <div class="line1"></div>
                                <div class="line-detail"><?php echo $this->lang->line('page_about_ppf_section1_imgtxt5', FALSE); ?></div>
                            </div>
                        </div><!--inner-img4-->
                    </div><!--inner-detail-img-ppt-->
                </div><!--detail-img-ppt-->
            </div>
            <?php echo $this->lang->line('page_about_ppf_section1_desc2', FALSE); ?>
        </div>
        <div class="row type-car">
            <ul>
                <li>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/q01.png">
                    <p><?php echo $this->lang->line('page_about_ppf_section1_feature1', FALSE); ?></p>
                </li>
                <li>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/q02.png">
                    <p><?php echo $this->lang->line('page_about_ppf_section1_feature2', FALSE); ?></p>
                </li>
                <li>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/q03.png">
                    <p><?php echo $this->lang->line('page_about_ppf_section1_feature3', FALSE); ?></p>
                </li>
                <li>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/q04.png">
                    <p><?php echo $this->lang->line('page_about_ppf_section1_feature4', FALSE); ?></p>
                </li>
                <li>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/q05.png">
                    <p><?php echo $this->lang->line('page_about_ppf_section1_feature5', FALSE); ?></p>
                </li>
            </ul>
        </div>
        <div class="row w-search">
            <div class="topic">
                <p class="title-page"><?php echo $this->lang->line('page_about_ppf_section2_title', FALSE); ?></p>
            </div>

            <div class="row serial">
                <form id="formSerial2">
                    <input id="input-serial-2" class="long"
                           placeholder="<?php echo $this->lang->line('page_about_ppf_section2_input', FALSE); ?>">
                    <a class="btn-serial btn-serial-2" data-fancybox="" data-src="#hidden-content"
                       href="javascript:;"><?php echo $this->lang->line('page_about_ppf_section2_search', FALSE); ?> <i
                                class="fa fa-search" aria-hidden="true"></i></a>
                </form>
            </div>
            <p><?php echo $this->lang->line('page_about_ppf_section2_desc', FALSE); ?></p>
        </div>
    </div>
</div><!--box_about_ppf-->
<div class="clr box_vdo">
    <div class="wrap">
        <div class="topic2">
            <p class="title-page"><?php echo $this->lang->line('page_about_ppf_section3_title', FALSE); ?></p>
        </div>
        <div class="vdo">
            <p style="margin:30px 0; color: #fff; font-size: 1.3em;"
               align="center"><?php echo $this->lang->line('page_about_ppf_section3_subtitle', FALSE); ?></p>
            <div id="myList"></div>
        </div>
    </div>
</div>
<div class="clr box_how_ppf">
    <div class="wrap">
        <div class="topic2">
            <p class="title-page"><?php echo $this->lang->line('page_about_ppf_section4_title', FALSE); ?></p>
        </div>
        <div class="list-ppf">
            <ul>
                <li>
                    <div class="img-vdo">
                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/works01.jpg">
                    </div>
                    <div class="detail">
                        <h3><?php echo $this->lang->line('page_about_ppf_section4_item1_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section4_item1_desc', FALSE); ?></p>
                    </div>
                </li>
                <li>
                    <div class="img-vdo">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/RDGvFgI_Gp4"
                                frameborder="0" allowfullscreen=""></iframe>
                    </div>
                    <div class="detail">
                        <h3><?php echo $this->lang->line('page_about_ppf_section4_item2_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section4_item2_desc', FALSE); ?></p>
                    </div>
                </li>
                <li>
                    <div class="img-vdo">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/9wy5r2dliuM"
                                frameborder="0" allowfullscreen=""></iframe>
                    </div>
                    <div class="detail">
                        <h3><?php echo $this->lang->line('page_about_ppf_section4_item3_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section4_item3_desc', FALSE); ?></p>
                    </div>
                </li>
                <li>
                    <div class="img-vdo">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/SBNK67sYAA4"
                                frameborder="0" allowfullscreen=""></iframe>
                    </div>
                    <div class="detail">
                        <h3><?php echo $this->lang->line('page_about_ppf_section4_item4_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section4_item4_desc', FALSE); ?></p>
                    </div>
                </li>
                <li>
                    <div class="img-vdo">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/n0SmqCsOKcI?start=36"
                                frameborder="0" allowfullscreen=""></iframe>
                    </div>
                    <div class="detail">
                        <h3><?php echo $this->lang->line('page_about_ppf_section4_item5_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section4_item5_desc', FALSE); ?></p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="clr box_about_ppf2">
    <div class="wrap">
        <div class="topic2">
            <p class="title-page"><?php echo $this->lang->line('page_about_ppf_section5_title', FALSE); ?></p>
        </div>
        <p style="margin:30px 0 40px;"
           align="center"><?php echo $this->lang->line('page_about_ppf_section5_subtitle', FALSE); ?></p>
        <div class="clr box-1">
            <figure>
                <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/a-ppf1.jpg">
            </figure>
            <div class="detail">
                <h3><?php echo $this->lang->line('page_about_ppf_section5_topic1_title', FALSE); ?></h3>
                <p><?php echo $this->lang->line('page_about_ppf_section5_topic1_desc', FALSE); ?></p>
            </div>
        </div>
        <div class="clr box-2">
            <div class="col-3">
                <div class="inner-box">
                    <h3><?php echo $this->lang->line('page_about_ppf_section5_topic2_title', FALSE); ?></h3>
                    <p><?php echo $this->lang->line('page_about_ppf_section5_topic2_desc', FALSE); ?></p>
                </div>
            </div>
            <div class="col-3">
                <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/a-ppf2.jpg">
            </div>
            <div class="col-3">
                <div class="inner-box">
                    <h3><?php echo $this->lang->line('page_about_ppf_section5_topic3_title', FALSE); ?></h3>
                    <p><?php echo $this->lang->line('page_about_ppf_section5_topic3_desc', FALSE); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clr box_protection">
    <div class="wrap">
        <div class="left">
            <h2>PAINT<span>PROTECTION</span><span>FILM</span></h2>
        </div>
        <div class="right">
            <div class="clr box-p">
                <figure>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/a-ppf3.jpg">
                </figure>
                <div class="detail">
                    <div class="inner-box">
                        <h3><?php echo $this->lang->line('page_about_ppf_section5_topic4_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section5_topic4_desc', FALSE); ?></p>
                    </div>
                </div>
            </div>
            <div class="clr box-p">
                <figure>
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/a-ppf4.jpg">
                </figure>
                <div class="detail">
                    <div class="inner-box">
                        <h3><?php echo $this->lang->line('page_about_ppf_section5_topic5_title', FALSE); ?></h3>
                        <p><?php echo $this->lang->line('page_about_ppf_section5_topic5_desc', FALSE); ?></p>
                    </div>
                </div>
            </div>
            <div class="clr box_adhesive">
                <div class="topic2">
                    <p class="title-page"
                       style="color:#f7bb00;"><?php echo $this->lang->line('page_about_ppf_section5_topic6_title', FALSE); ?></p>
                </div>
                <p><?php echo $this->lang->line('page_about_ppf_section5_topic6_desc', FALSE); ?></p>
            </div>
        </div>
    </div>
</div>
<div class="clr box_caution">
    <div class="wrap">
        <div class="topic2" style="text-align:left;">
            <p class="title-page"
               style="color:#f7bb00;"><?php echo $this->lang->line('page_about_ppf_section6_title', FALSE); ?></p>
        </div>
        <div class="clr inner-caution">
            <div class="left">
                <ul>
                    <?php echo $this->lang->line('page_about_ppf_section6_desc', FALSE); ?>
                </ul>
            </div>
            <div class="right">
                <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/car3.png">
            </div>
        </div>
    </div>
</div>

<!--------------------------popup------------------------------------>
<div style="display: none;" id="hidden-content">
    <h2>Product Authentication</h2>
    <p class="found">The product with your Serial Number has been imported and applied on your car on the date shown in
        the search result here.</p>
    <p class="notfound" style="font-size: 15px;margin: 30px 0 15px;"><span
                style="display: block; text-align:center; font-weight: bold; font-size: 26px;margin: 15px 0 15px;">Serial Not Found</span>The
        Serial Number you entered does not exist or may be wrong. Please check and enter again. If it still cannot be
        found, please contact info@sensha-world.com.</p>
    <table class="found">
        <tr>
            <th><em>Serial No.</em></th>
            <td><em class="data-serial"></em></td>
        </tr>
        <tr>
            <th><em>Lot No.</em></th>
            <td><em class="data-lot"></em></td>
        </tr>
        <tr>
            <th><em>Export Date</em></th>
            <td><em class="data-export-date"></em></td>
        </tr>
        <tr>
            <th><em>Country</em></th>
            <td><em class="data-country"></em></td>
        </tr>
        <tr>
            <th><em>Arrival Date</em></th>
            <td><em class="data-arrival-date"></em></td>
        </tr>
        <tr>
            <th><em>Franchise Name</em></th>
            <td><em class="data-shop-name"></em></td>
        </tr>
        <tr>
            <th><em>Sold Date</em></th>
            <td><em class="data-shipment-arrival-date"></em></td>
        </tr>
        <tr>
            <th><em>Applied Date</em></th>
            <td><em class="data-operation-date"></em></td>
        </tr>
        <tr>
            <th><em>Brand</em></th>
            <td><em class="data-car-brand"></em></td>
        </tr>
        <tr>
            <th><em>Model</em></th>
            <td><em class="data-car-series"></em></td>
        </tr>
        <tr>
            <th><em>Color</em></th>
            <td><em class="data-car-color"></em></td>
        </tr>
    </table>
</div>

<script>

    $('.btn-serial-2').click(function () {
        if ($('#input-serial-2').val() == '') {
            alert('Please fill serial');
            return false;
        }
        getData($('#input-serial-2').val());
    });

    function getData(v) {
        $('.found').hide();
        $('.notfound').hide();

        $.get("<?php echo base_url("page/checkSerial/");?>" + v, function (msg) {
            if (msg != 0 && msg != 'null') {
                data = JSON.parse(msg);
                $('table').show();
                var serial_value = data.serial_number;
                if (data.serial_merge != '') {
                    serial_value = data.serial_merge;
                }
                $('.data-serial').text(serial_value);
                $('.data-lot').text(data.lot_number);
                $('.data-export-date').text(data.export_date);
                $('.data-country').text(data.country);
                $('.data-arrival-date').text(data.arrival_date);
                $('.data-shop-name').text(data.shop_name);
                $('.data-shipment-arrival-date').text(data.shipment_arrival_date);
                $('.data-operation-date').text(data.operation_date);
                $('.data-car-brand').text(data.car_brand);
                $('.data-car-series').text(data.car_series);
                $('.data-car-color').text(data.car_color);

                // $('.found').show();
                // $('#hidden-content').show();
                $('.found').show();

                // console.log(data);
            } else {
                $('.notfound').show();
            }
        });

        // $.ajax({
        //   method: "GET",
        //   url: "<?php echo base_url("page/checkSerial/");?>"+v,
        //   // data: {'getDataFormSerial':v }
        // })
        // .success(function( msg ) {
        //   // console.log(msg);
        //   if(msg != 0 && msg != 'null'){
        //     data = JSON.parse(msg);
        //     $('table').show();
        //     $('.data-serial').text(data.serial_number);
        //     $('.data-lot').text(data.lot_number);
        //     $('.data-export-date').text(data.export_date);
        //     $('.data-country').text(data.country);
        //     $('.data-arrival-date').text(data.arrival_date);
        //     $('.data-shop-name').text(data.shop_name);
        //     $('.data-shipment-arrival-date').text(data.shipment_arrival_date);
        //     $('.data-operation-date').text(data.operation_date);
        //     $('.data-car-brand').text(data.car_brand);
        //     $('.data-car-series').text(data.car_series);
        //     $('.data-car-color').text(data.car_color);
        //
        //     // $('.found').show();
        //     // $('#hidden-content').show();
        //     $('.found').show();
        //
        //     // console.log(data);
        //   }
        //   else{
        //     $('.notfound').show();
        //   }
        //
        // })
        // .error(function(){
        //   // alert('system error.');
        //   // // $('#modalEventAdd').find('.alert-danger').show();
        // });
    }
</script>
