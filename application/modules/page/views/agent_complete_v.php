<div class="clr inner">
<div id="breadcrumbs" style="margin:15px 0;">
  <span><a href="<?php echo base_url();?>">HOME</a><span> <?php echo $this->lang->line('breadcrumb_sole_agent_complete', FALSE); ?></span></span>
</div>
</div>
  <div class="clr box_apply">
  <div class="wrap">
    <div class="topic2">
    <p class="title-page"><?php echo $this->lang->line('page_sole_agent_section4_title', FALSE); ?></p>
    </div>
    <div class="inner-apply">
          <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_sole_agent_complete_your_message', FALSE); ?></h2>
          <?php echo $this->lang->line('page_sole_agent_complete_thank_you', FALSE); ?>
  </div>
  </div>
</div>
