<!--<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span>Contact complete</span>
		</div>
	</div><!--inner-->
<!--	<div class="clr inner">
		Thank you
	</div><!--inner-->
<!--</div><!--container-->
<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_contact_complete', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="topic">
        <p class="title-page"><?php echo $this->lang->line('page_contact_complete_confirmation', FALSE); ?></p>
      </div>
      <div class="clr box_complete">
        <div class="left">
            <img src="<?php echo base_url("assets/sensha-theme/");?>images/img-claim.png">
        </div>
        <div class="right">
          <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_contact_complete_your_message', FALSE); ?></h2>
          <?php echo $this->lang->line('page_contact_complete_thank_you', FALSE); ?>
        </div>
      </div>
    </div><!--layout-contain-->
  </div><!--box-content-->
</div><!--inner-->
</div><!--container-->
