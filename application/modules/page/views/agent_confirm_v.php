<div class="clr inner">
<div id="breadcrumbs" style="margin:15px 0;">
  <span><a href="<?php echo base_url();?>">HOME</a><span> <?php echo $this->lang->line('breadcrumb_sole_agent_confirm', FALSE); ?></span></span>
</div>
</div>
  <div class="clr box_apply">
  <div class="wrap">
    <div class="topic2">
    <p class="title-page"><?php echo $this->lang->line('page_sole_agent_section4_title', FALSE); ?></p>
    </div>
    <p style="font-weight:600;text-align:center;"><?php echo $this->lang->line('page_sole_agent_section4_subtitle', FALSE); ?></p>
    <div class="inner-apply">
      <form method="post" action="<?php echo base_url("page/agent_complete");?>">
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_contact_name', FALSE); ?></label>
	      <div class="r-input">
	        <p><?php echo $this->input->post('name');?></p>
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_contact_email', FALSE); ?></label>
	      <div class="r-input">
	        <p><?php echo $this->input->post('email');?></p>
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_contact_tel', FALSE); ?></label>
	      <div class="r-input">
	        <p><?php echo $this->input->post('tel');?></p>
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_contact_country', FALSE); ?></label>
	      <div class="r-input">
	        <p><?php echo $this->input->post('country');?></p>
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/11.png"><?php echo $this->lang->line('page_contact_message', FALSE); ?></label>
	      <div class="r-input">
	        <p><?php echo $this->input->post('msg');?></p>
	      </div>
	      </div>
	      <div class="row-btn"><button type="submit" class="b-blue"><?php echo $this->lang->line('page_contact_submit', FALSE); ?></button></div>
	      <input type="hidden" value="<?php echo $this->input->post('name');?>" name="name">
	      <input type="hidden" value="<?php echo $this->input->post('email');?>" name="email">
	      <input type="hidden" value="<?php echo $this->input->post('tel');?>" name="tel">
	      <input type="hidden" value="<?php echo $this->input->post('country');?>" name="country">
	      <input type="hidden" value="<?php echo $this->input->post('msg');?>" name="msg">
      </form>
  </div>
  </div>
</div>
