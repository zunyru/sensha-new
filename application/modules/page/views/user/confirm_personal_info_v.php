<div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_check_account', FALSE); ?></span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="layout-contain">
				    <div class="clr box_form">
						<div class="topic">
							<p class="title-page"><?php echo $this->lang->line('page_check_account_confirm_title', FALSE); ?></p>
					    </div>
						<div class="box-inner">
							<div class="r-inline" style="display: none;">
								<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_account_info_id', FALSE); ?></label>
								<div class="r-input">
									<p></p>
								</div>
							</div>
							<div class="r-inline">
								<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_account_info_email', FALSE); ?></label>
								<div class="r-input">
									<p><?php echo $this->ion_auth->user()->row()->email;?></p>
								</div>
							</div>
							<!--<div class="r-inline">
								<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_account_info_telephone', FALSE); ?></label>
								<div class="r-input">
									<p><?php echo $this->input->post('phone');?></p>
								</div>
							</div>-->
							<div class="r-inline">
								<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_account_info_password', FALSE); ?></label>
								<div class="r-input">
									<p></p>
								</div>
							</div>
							<div class="r-inline">
								<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_account_info_country', FALSE); ?></label>
								<div class="r-input">
									<p><?php echo $this->input->post('country');?></p>
								</div>
							</div>
							<?php if($this->input->post('country') == 'Japan'):?>
							<!--<div class="r-inline">
								<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/06.png"><?php echo $this->lang->line('page_account_info_state', FALSE); ?></label>
								<div class="r-input">
									<p><?php echo $this->input->post('state');?></p>
								</div>
							</div>-->
						<?php endif;?>
							<div class="row-btn">
							  <a href="<?php echo base_url("page/user/complete_personal_info");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_check_account_confirm', FALSE); ?></a>
							</div>
					   </div>
					</div>


			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
