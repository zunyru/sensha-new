
<div class="container bg">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_wishlist', FALSE); ?></span>
  </div>
    <div class="layout-contain">
    <div class="title-wish">
       <ul>
         <li>
           <p><?php echo $this->lang->line('page_wishlist_wishlist', FALSE); ?></p>
         </li>
         <!--<li><span class="txt-b2"><?php echo $this->lang->line('page_cart_quantity', FALSE); ?></span></li>
         <li><span class="txt-b2"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span></li>
         <li><span class="txt-b2"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span></li>-->
       </ul>
    </div>
    <div class="box-wish">
      <div class="clr product-list">
        <ul>
          <?php foreach($products as $item):?>
            <?php
            $product_images = explode(',', $item->image);
            ?>
            <!--<li>
              <div class="clr c1">
                <div class="pic-product">
                  <img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>">
                </div>
              </div>
              <div class="clr c2">
                <div class="product-name">
                  <p class="name-p"><?php echo $item->product_name;?></p>
                  <div class="available">
                    <a href="" onclick="delete_wishlist('<?php echo $item->id;?>'); return false;" class="b-delete"><?php echo $this->lang->line('page_wishlist_delete', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-delete.png"></a>
                  </div>
                </div>
                <div class="amount">
                  <select>
                    <option value="0">1</option>
                    <option value="1">2</option>
                    <option value="2">3</option>
                    <option value="3">4</option>
                    <option value="4">5</option>
                  </select>
                </div>
                <div class="box-price">
                  <div class="inner-p">
                    <span class="txt-b2"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
                    <p class="txt-red"><?php echo @number_format($item->global_price);?> Yen</p>
                    <div class="row"><span class="import"><?php echo $this->lang->line('page_product_detail_import_shipping', FALSE); ?></span><span class="tax"><?php echo @number_format($item->import_shipping);?> Yen</span></div>
                    <div class="row"><span class="import"><?php echo $this->lang->line('page_product_detail_import_duty', FALSE); ?></span><span class="tax"><?php echo @number_format($item->import_tax);?> Yen</span></div>
                    <div class="btn">
                      <a href="cart.php" class="b-cart"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> &nbsp;<img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
                    </div>
                  </div>
                </div>
                <div class="clr box-type">
                   <div class="inner-p">
                    <span class="txt-b2"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
                    <p class="txt-red"><?php echo @number_format($item->sa_price);?> Yen</p>
                    <div class="btn">
                      <a href="cart.php" class="b-cart"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> &nbsp;<img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
                    </div>
                  </div>
                </div>
              </div>
            </li>-->
			<li>
				<a href="<?php echo base_url("$coutry_iso"."page/product_detail/$item->product_type/$item->product_id");?>">
					<figure><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>" alt=""/></figure>
					<div class="detail">
						<p class="name-p"><?php echo $item->product_name;?></p>
						<p class="quantity"><?php echo $item->caution;?></p>
						<?php if($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')) )):?>
							<?php
							$this->db->where('product_id', $item->product_id);
							$product_domestic = $this->datacontrol_model->getRowData('product_general');
							?>
							<?php if($product_domestic->is_active == 1):?>
								<div class="clr p-amount">
									<span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
									<div class="price">
										<?php echo number_format($product_domestic->sa_price);?> Yen
									</div>
								</div>
							<?php endif;?>
						<?php endif;?>
						<?php //if($this->ion_auth->in_group(array('SA')) || (!$this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')) )):?>
						<?php 
							$user_country = $this->ion_auth->user()->row()->country;							
							if( ($this->ion_auth->in_group(array('SA'))&& !in_array($user_country, array('Japan', 'JapanA', 'JapanB'))) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')) )):?>
							<div class="clr p-amount">
								<span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
								<div class="price">
									<?php echo number_format($item->global_price);?> Yen
								</div>
							</div>
						<?php endif;?>

					</div>
				</a>
                  <div class="available">
                    <a href="" onclick="delete_wishlist('<?php echo $item->id;?>'); return false;" class="b-delete"><?php echo $this->lang->line('page_wishlist_delete', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-delete.png"></a>
                  </div>
			</li>
          <?php endforeach;?>
        </ul>
      </div>
            </div>
  </div><!--layout-contain-->
</div><!--inner-->
</div><!--container-->


<script>
function delete_wishlist(id){
  $.ajax({
    url: '<?php echo base_url("page/user/delete_wishlist/");?>'+id,
    type:'GET',
    success: function(data){
      window.location = '';
    }
  });
}
</script>
