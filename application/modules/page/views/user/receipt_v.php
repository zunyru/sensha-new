<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $this->lang->line('pdf_receipt_title', FALSE); ?></title>
</head>
<style type="text/css">
	body {
		font-size:14px;
		color: #0070c0;
	}
	.txt-red{color:#FF0000}
	table{border:1}
	table {
	  width: 100%;
	}
	td, th {
	  text-align: left;
	}
</style>
<body>

	<table style="border:4px solid #0070c0;" align="center">
		<tbody>
			<tr>
				<td style="width: 20px;"></td>
				<td style="width: 500px;">
					<table>
						<tbody>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4" style="text-align:center;border-top:1px solid #0070c0;border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="text-align: center;font-size:25px;border-left:1px solid #0070c0;border-right:1px solid #0070c0;"><strong><?php echo $this->lang->line('pdf_receipt_title', FALSE); ?></strong> </td>
							</tr>
							<tr>
								<td colspan="4" style="text-align:center;border-bottom:1px solid #0070c0;border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;">
								<td colspan="2" style="border-left:1px solid #0070c0;">&nbsp;&nbsp;&nbsp;<strong><?php echo $this->lang->line('pdf_receipt_invoice_no', FALSE); ?></strong></td>
								<td style="color:black"><?php echo date('Ymd', strtotime($sales_history_total->sale_date));?> - <?php echo $sales_history_total->create_by;?></td>
								<td style="border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
							<?php if($coutry_iso=='jp'){ ?>
								<td colspan="3" style="color:black;border-left:1px solid #0070c0;">&nbsp;&nbsp;&nbsp;<?php echo $shipping_info->company;?>様</td>
							<?php }else{ ?>
								<td colspan="2" style="border-left:1px solid #0070c0;">&nbsp;&nbsp;&nbsp;<strong><?php echo $this->lang->line('pdf_receipt_company_name', FALSE); ?></strong></td>
								<td style="color:black"><?php echo $shipping_info->company;?></td>							
							<?php } ?>
								<td style="border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;">&nbsp;&nbsp;&nbsp;<strong><?php echo $this->lang->line('pdf_receipt_transaction_no', FALSE); ?></strong> <strong style=""><?php echo $sales_history_total->shipment_id;?></strong></td>
							</tr>
							<tr>
								<td colspan="2" style="color:#000;border-left:1px solid #0070c0;">&nbsp;&nbsp;&nbsp;<strong>¥ <?php echo number_format($sales_history_total->sale_total_amount + $sales_history_total->admin_fee_amount + $sales_history_total->vat_amount);?></strong></td>
								<td></td>
								<td style="border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="text-align:center;font-size:18px;border-left:1px solid #0070c0;border-right:1px solid #0070c0;"><strong><?php echo $this->lang->line('pdf_receipt_received', FALSE); ?></strong> </td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;border-bottom:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4" style="text-align: center;font-size:18px;border-left:1px solid #0070c0;border-right:1px solid #0070c0;"><strong><?php echo $this->lang->line('pdf_receipt_date_issued', FALSE); ?><?php echo date('M jS, Y', strtotime($sales_history_total->create_date))?></strong> </td>
							</tr>
							<tr>
								<td colspan="4" style="border-left:1px solid #0070c0;border-right:1px solid #0070c0;border-bottom:1px solid #0070c0;"></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td style="width: 20px;"></td>
				<td style="width: 350px;">
					<table>
						<tbody>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4"><img src="<?php echo base_url('assets/sensha-theme/images/logo.png');?>" width="220" height="73" alt=""/></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4"><strong><?php echo $this->lang->line('pdf_receipt_address1', FALSE); ?></strong></td>
							</tr>
							<tr>
								<td colspan="4"><strong><?php echo $this->lang->line('pdf_receipt_address2', FALSE); ?></strong></td>
							</tr>
							<tr>
								<td colspan="4"><strong><?php echo $this->lang->line('pdf_receipt_address3', FALSE); ?></strong></td>
							</tr>
							<tr>
								<td colspan="4"><strong><?php echo $this->lang->line('pdf_receipt_tel', FALSE); ?></strong></td>
							</tr>
							<tr>
								<td colspan="4"><strong><?php echo $this->lang->line('pdf_receipt_fax', FALSE); ?></strong><br></td>
							</tr>
							<tr>
								<td colspan="4"><strong><?php echo $this->lang->line('pdf_receipt_payment_received', FALSE); ?></strong><br></td>
							</tr>
							<tr>
								<td style="color:#000; width: 20px; text-align: center;"><div style="border:2px solid #000;width:20px;text-align:center;">x</div> </td>
								<td colspan="3"><strong><?php echo $sales_history_total->payment_method;?></strong></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="3"><strong><?php echo $this->lang->line('pdf_receipt_approved', FALSE); ?></strong></td>
								<td><img src="<?php echo base_url('assets/sensha-theme/images/image2.jpg');?>" width="72" height="70" alt=""/></td>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td style="width: 20px;"></td>
			</tr>
		</tbody>
	</table>
</body>
</html>
