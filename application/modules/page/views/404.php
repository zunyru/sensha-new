<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span>404 PAGE NOT FOUND</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="box-content" style="width: calc(100%);">
			<div class="layout-contain">
		    	<div class="clr box_form">
					<div class="topic">
						<p class="title-page">Sorry, this page isn't available.</p>
					</div>
					<div class="clr box-success" style="text-align: center;margin-top:50px;">
						<p style="font-size: 16px;font-weight: 700;margin-bottom: 40px;">The link you followed may be broken, or the page may have been removed.</p>
					    <h3 style="font-size:98px;font-weight: 700;font-family: 'Oswald', sans-serif;color:#004ea1;">404 </h3>
						<p style="font-size: 35px;font-weight: 700;margin-bottom:30px;">PAGE NOT FOUND</p>
					</div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script>
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->
