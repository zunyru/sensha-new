<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_register_confirm', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_register_confirm_title', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/email-authentication.png"><?php echo $this->lang->line('page_register_confirm_authenticate', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->lang->line('page_register_confirm_authenticate_via', FALSE); ?><?php echo $this->input->post('register_by');?></p>
						</div>
					</div>
					<?php if($this->input->post('register_by') == 'email'):?>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_register_email', FALSE); ?></label>
							<div class="r-input">
								<p><?php echo $this->input->post('email');?></p>
							</div>
						</div>
					<?php else:?>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_register_tel', FALSE); ?></label>
							<div class="r-input">
								<p><?php echo $this->input->post('phone');?></p>
							</div>
						</div>
					<?php endif;?>

					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_register_password', FALSE); ?></label>
						<div class="r-input">
							<p>***********</p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_register_country', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo ucwords(str_replace("_"," ", $this->input->post('country')))?></p>
						</div>
					</div>
					<!--<?php if($this->input->post('country') == 'Japan'):?>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/06.png">State</label>
							<div class="r-input">
								<p><?php echo $this->input->post('state');?></p>
							</div>
						</div>
					<?php endif;?>-->
					<div style="text-align: center;margin-top:30px;border-top: 1px solid #d4d4d4;padding-top: 20px;">
						<p><?php echo $this->lang->line('page_register_confirm_subtitle', FALSE); ?></p>
					</div>
					<div class="row-btn">
						<?php if($this->input->post('register_by') == 'email'):?>
							<a href="<?php echo base_url("$coutry_iso"."page/register/register_successfully");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_register_confirm_confirm', FALSE); ?></a>
						<?php else:?>
							<a href="<?php echo base_url("$coutry_iso"."page/register/register_otp");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_register_confirm_confirm', FALSE); ?></a>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div><!--layout-contain-->
	</div><!--inner-->
</div><!--container-->
