<div class="container">
    <div class="clr inner">
        <div id="breadcrumbs">
			<span><a href="<?php echo base_url(); ?>">Home</a><span><?php echo $this->lang->line('breadcrumb_register', FALSE); ?></span>
        </div>
    </div><!--inner-->
    <form method="post" id="form"
          action="<?php echo base_url("$coutry_iso" . "page/register/register_confirmation"); ?>">
        <div class="clr inner">
            <div class="layout-contain">
                <div class="clr box_form">
                    <div class="topic">
                        <p class="title-page"><?php echo $this->lang->line('page_register_title', FALSE); ?></p>
                    </div>
                    <div class="box-paragraph">
                        <p><?php echo $this->lang->line('page_register_subtitle', FALSE); ?></p>
                    </div>
                    <div class="box-inner">
                        <div class="clr inline-radio">
                            <div class="left">
                                <label class="">
                                    <input type="radio" name="register_by" value="email" checked
                                           required> <?php echo $this->lang->line('page_register_via_email', FALSE); ?>
                                </label>
                            </div>
                            <div class="left">
                                <label class="">
                                    <input type="radio" name="register_by" value="SMS"
                                           required> <?php echo $this->lang->line('page_register_via_sms', FALSE); ?>
                                </label>
                            </div>
                        </div>
                        <div class="r-inline email">
                            <label class="label"><img
                                        src="<?php echo base_url("assets/sensha-theme/"); ?>images/04.png"><?php echo $this->lang->line('page_register_email', FALSE); ?>
                            </label>
                            <div class="r-input">
                                <input type="text"
                                       placeholder="<?php echo $this->lang->line('page_register_input_email', FALSE); ?>"
                                       class="form-control" name="email" required>
                            </div>
                        </div>
                        <div class="r-inline sms">
                            <label class="label"><img
                                        src="<?php echo base_url("assets/sensha-theme/"); ?>images/07.png"><?php echo $this->lang->line('page_register_tel_code', FALSE); ?>
                            </label>
                            <div class="r-input">
                                <!-- <input type="text" placeholder="Please input your Telephone" class="form-control" name="phone"> -->
                                <select name="countryCode" id="" class="form-control">
                                    <option data-countryCode="GB" value="44" Selected>UK (+44)</option>
                                    <option data-countryCode="US" value="1">USA (+1)</option>
                                    <optgroup label="Other countries">
                                        <?php
                                        $not_c = ['JapanA', 'JapanB', 'United_Kingdom', 'United_States'];
                                        foreach ($countries as $item):
                                            ?>
                                            <?php if (!in_array($item->nicename, $not_c)): ?>
                                            <option data-countryCode="<?= $item->iso ?>"
                                                    value="<?= $item->phonecode ?>">
                                                <?= ucwords(str_replace("_", " ", $item->nicename)) . " (+" . $item->phonecode . ")" ?></option>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="r-inline sms">
                            <label class="label"><img
                                        src="<?php echo base_url("assets/sensha-theme/"); ?>images/07.png"><?php echo $this->lang->line('page_register_tel', FALSE); ?>
                            </label>
                            <div class="r-input">
                                <input type="text"
                                       placeholder="<?php echo $this->lang->line('page_register_input_tel', FALSE); ?>"
                                       class="form-control" name="phone">
                            </div>
                        </div>
                        <div class="r-inline">
                            <label class="label"><img
                                        src="<?php echo base_url("assets/sensha-theme/"); ?>images/08.png"><?php echo $this->lang->line('page_register_password', FALSE); ?>
                            </label>
                            <div class="r-input">
                                <input type="password"
                                       placeholder="<?php echo $this->lang->line('page_register_input_password', FALSE); ?>"
                                       class="form-control" name="password" required>
                            </div>
                        </div>
                        <div class="r-inline">
                            <label class="label"><img
                                        src="<?php echo base_url("assets/sensha-theme/"); ?>images/01.png"><?php echo $this->lang->line('page_register_country', FALSE); ?>
                            </label>
                            <div class="r-input">
                                <select class="form-control" name="country" required>
                                    <option value=""><?php echo $this->lang->line('page_register_country_select', FALSE); ?></option>
                                    <?php
                                    $not_c = ['JapanA', 'JapanB'];
                                    foreach ($countries as $item):
                                        ?>
                                        <?php if (!in_array($item->nicename, $not_c)): ?>
                                        <option value="<?php echo $item->nicename; ?>"><?php echo ucwords(str_replace("_", " ", $item->nicename)) ?></option>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="r-inline japan-only" style="display:none;">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/06.png">State</label>
						<div class="r-input">
							<select class="form-control" name="state">
								<option value="0">If Japan</option>
								<option value="1">2</option>
								<option value="2">3</option>
								<option value="3">4</option>
								<option value="4">5</option>
							</select>
						</div>
					</div> -->
                        <div class="row-btn">
                            <button type="submit" class="b-blue"><img
                                        src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-save.png"
                                        style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_register_save', FALSE); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div><!--layout-contain-->
        </div><!--inner-->
    </form>

</div><!--container-->
<style>
    .error {
        color: red;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script>
<script>
    $(function () {

        $("#form").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: `<?php echo base_url("page/register/register_email_check"); ?>`,
                        type: "post",
                        data:
                            {
                                email: function () {
                                    return $('#form :input[name="email"]').val();
                                }
                            }
                    }
                },
                password: {
                    required: true,
                },
                country: {
                    required: true,
                },
                countryCode: {
                    required: true,
                },
                phone: {
                    required: true,
                    remote: {
                        url: `<?php echo base_url("page/register/register_phone_check"); ?>`,
                        type: "post",
                        data:
                            {
                                phone: function () {
                                    return $('#form :input[name="phone"]').val();
                                }
                            }
                    }
                }
            },
            messages: {
                email: {
                    remote: "This e-mail address is unavailable for registration",
                },
                phone: {
                    remote: "This phone is unavailable for registration",
                }
            },
        });

        $('.sms').hide();
        $('input[name="register_by"]').change(function () {
            if ($(this).val() == 'email') {
                $('.sms').hide();
                $('.email').show();

                $('input[name="phone"]').removeAttr('required', '')
                $('input[name="email"]').attr('required', '')
            }
            if ($(this).val() == 'SMS') {
                $('.sms').show();
                $('.email').hide();

                $('input[name="phone"]').attr('required', '')
                $('input[name="email"]').removeAttr('required', '')
            }
        });
    });
    // $('select[name="country"]').change(function(){
    // 	if($(this).val() == 'Japan'){
    // 		$('.japan-only').show();
    // 	}
    // 	else{
    // 		$('.japan-only').hide();
    // 	}
    // });
</script>
