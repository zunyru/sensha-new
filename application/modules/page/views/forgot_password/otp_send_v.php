<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs">
      <span><a href="index.php">Home</a><span>Password reset</span>
    </div>
  </div><!--inner-->
  <div class="clr inner">
    <div class="layout-contain">
      <div class="clr box_form">
        <div class="topic">
          <p class="title-page">Set your new password</p>
        </div>
        <div class="box-paragraph">
          <p>Please confirm the password change by entering the OTP number from your phone number.</p>
        </div>
        <div class="box-inner">
          <div class="r-inline">
            <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png">OTP</label>
            <div class="r-input">
              <input type="text" placeholder="Please enter your OTP Number" class="form-control">
            </div>
          </div>

          <div class="row-btn">
            <a href="#" class="c-gray"><img src="<?php echo base_url("assets/sensha-theme/");?>images/mail.png" style="width:18px;margin-right:8px;">Send again</a>
            <a href="password-successfully.php" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;">Confirm</a>
          </div>
        </div>
      </div>
    </div><!--layout-contain-->
  </div><!--inner-->
</div><!--container-->
