<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_cancel', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="box-progress">
        <div class="progress-row">
          <div class="progress-step">
            <button type="button" class="btn-circle">1</button>
            <p>Cart item& Delivery</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">2</button>
            <p>Shipping Address</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">3</button>
            <p>Confirmation& Payment</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">4</button>
            <p>Payment Pending</p>
          </div>
        </div>
      </div>
      <div class="topic">
        <p class="title-page">Payment Pending - Your Order is Being Processed</p>
      </div>
      <div class="clr box_complete">
        <div class="left">
            <img src="<?php echo base_url("assets/sensha-theme/");?>images/img-complete.png">
        </div>
        <div class="right">
          <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_cancel_your_order', FALSE); ?></h2>
          Thank you for your order! Your payment is currently pending and is being processed. Once the payment is confirmed, we will proceed with fulfilling your order. Please check your email for further updates. If you don’t hear from us within the next few hours, feel free to contact us through our contact page for assistance with your order status.
        </div>
      </div>
    </div><!--layout-contain-->
  </div><!--box-content-->
</div><!--inner-->
</div><!--container-->
