<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{
    var $user_lang;
    var $coutry_iso;

    function __construct()
    {
        parent::__construct();
        $this->load->model('database/datacontrol_model');
        // delete_cookie('user_language');
        // $this->user_lang = get_cookie('user_language');
        $this->user_lang = 'Global';
        $this->coutry_iso = "";

        if (!$this->ion_auth->logged_in()) {
            if ($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page') {
                $this->db->where('iso', strtoupper($this->uri->segment(1)));
                $r = $this->datacontrol_model->getRowData('countries');
                $this->user_lang = $r->nicename;
                $this->coutry_iso = strtolower($r->iso);
            }
        } else {
            if ($this->session->has_userdata('user_language')) {
                $this->user_lang = $this->session->userdata('user_language');
            }

            $this->db->where('nicename', $this->user_lang);
            $r = $this->datacontrol_model->getRowData('countries');
            $this->coutry_iso = strtolower($r->iso);

            if ($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page' && $this->uri->segment(1) != $this->coutry_iso) {
                $segs = $this->uri->segment_array();
                $segs[1] = $this->coutry_iso;
                // echo $new_uri = str_replace("/".$this->uri->segment(1), $this->coutry_iso."/", uri_string());
                redirect(base_url($segs), 'refresh');
            }
        }


        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
            $this->coutry_iso = "";
        }

        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $user_country = $this->ion_auth->user()->row()->country;
        $data['noindex'] = true;
        $data['countries'] = $this->datacontrol_model->getAllDataWhere('countries', ['zone is NOT NULL' => NULL]);
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'register/register_v';
        $this->load->view('front_template', $data);
    }

    public function register_confirmation()
    {
        if (empty($this->input->post('country'))) {
            return redirect(base_url('page/register'))->withInput()->with('error', lang('Select country'));
        }
        $this->session->set_userdata('register', $this->input->post());
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'register/register_confirmation_v';
        $this->load->view('front_template', $data);
    }

    public function register_email_check()
    {
        $email = $this->input->post('email');
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        $row = $query->row();
        if (empty($row)) {
            echo "true";
            exit();
        }
        echo "false";
        exit();
    }

    public function register_phone_check()
    {
        $email = $this->input->post('phone');
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        $row = $query->row();
        if (empty($row)) {
            echo "true";
            exit();
        }
        echo "false";
        exit();
    }

    public function register_otp()
    {
        $_POST = $this->session->userdata('register');
        // $six_digit_random_number = mt_rand(100000, 999999);
        $six_digit_random_number = $this->generateOTP();
        $this->session->set_userdata('otp', $six_digit_random_number);
        // $six_digit_random_number = '864486';

        require_once 'vendor/autoload.php';
        $basic = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
        $client = new \Nexmo\Client($basic);

        $countryCode = $this->input->post('countryCode');
        $phone = $this->input->post('phone');
        $firstCharacter = substr($phone, 0, 1);
        if ($firstCharacter == 0) {
            $phone = substr($phone, 1);
        }
        // if(strlen($phone) == 10){
        //   $phone = substr($phone, 1);
        // }


        // echo '+'.$countryCode.$phone;

        $message = $client->message()->send([
            'to'   => '+' . $countryCode . $phone,
            'from' => 'Sensha',
            'text' => "Your OTP is $six_digit_random_number"
        ]);

        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'register/register_confirmation_otp_v';
        $this->load->view('front_template', $data);
    }

    function generateOTP($length = 6)
    {
        $chars = '0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    public function check_otp()
    {
        // echo $this->session->userdata('otp');
        $otp = $this->session->userdata('otp');
        if ($otp == $this->input->post('otp')) {
            echo json_encode(array('err' => 0));
        } else {
            echo json_encode(array('err' => 1));
        }
    }

    public function register_successfully()
    {
        $_POST = $this->session->userdata('register');
        // print_r($_POST);

        $email = strtolower($this->input->post('email'));
        $password = $this->input->post('password');
        if ($this->input->post('register_by') == 'SMS') {
            $email = strtolower($this->input->post('phone'));
        }

        $additional_data = array(
            'phone'   => $this->input->post('phone'),
            'country' => $this->input->post('country'),
        );

        $new_account = $this->ion_auth->register($email, $password, $email, $additional_data);

        $id = $new_account['id'];

        if ($new_account) {
            // @keep_log('signup', '', $email);

            if ($this->input->post('register_by') == 'email') {
                $activate_code = $this->ion_auth->user($id)->row()->activation_code;
                $msg = $email . ' 様<br><br>' . $this->lang->line('mail_register_content', FALSE) . '<br><br><a href="' . base_url("page/register/activateAccount/$id/$activate_code") . '">' . base_url("page/register/activateAccount/$id/$activate_code") . '</a>';
                mail_to(array("$email" => $this->input->post('name')), "Activate to your account.", $msg);

            } else {
                $this->ion_auth->update($id, array('active' => 1));
            }

            //send email to admin
            $email_admin = $this->config->item('contact_email');
            $user_id = $this->ion_auth->user($id)->row()->id;
            $user_country = $this->ion_auth->user($id)->row()->country;
            $msg_admin = "Content: 新たなユーザーの申し込みがありました。<br><br>ID : {$user_id}<br>COUNTRY : {$user_country}<br><br>詳細は管理画面よりご確認ください。<br>登録連絡先 : {$email}";
            mail_to(array("$email_admin" => $this->input->post('name')), "New GU registration.", $msg_admin);


            // echo json_encode(array('error' => 0, 'msg' => 'Signup success. Please confirm your email'));
        } else {
            // echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
        }

        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'register/register_successfully_v';
        $this->load->view('front_template', $data);
    }

    public function activateAccount($id, $code = FALSE, $send_back = true)
    {
        if ($code !== FALSE) {
            $activation = $this->ion_auth->activate($id, $code);
            if ($activation) {
                $user = $this->ion_auth->user($id)->row();
                $this->ion_auth->update_last_login($user->id);
                $this->ion_auth->clear_login_attempts($user->email);
                $this->ion_auth->trigger_events(array('post_login', 'post_login_successful'));
                $this->ion_auth->set_message('login_successful');
            }
        }

        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'register/register_complete_v';
        $this->load->view('front_template', $data);
    }
}
