<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('database/datacontrol_model');
    // delete_cookie('user_language');
    // $this->user_lang = get_cookie('user_language');
    $this->user_lang = 'Global';
    $this->coutry_iso = "";

    if(!$this->ion_auth->logged_in()){
      if($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page'){
        $this->db->where('iso', strtoupper($this->uri->segment(1)));
        $r = $this->datacontrol_model->getRowData('countries');
        $this->user_lang = $r->nicename;
        $this->coutry_iso = strtolower($r->iso);
      }
    }
    else{
      if($this->session->has_userdata('user_language')){
        $this->user_lang = $this->session->userdata('user_language');
      }
//
      $this->db->where('nicename', $this->user_lang);
      $r = $this->datacontrol_model->getRowData('countries');
      $this->coutry_iso = strtolower($r->iso);

      if($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page' && $this->uri->segment(1) != $this->coutry_iso){
        $segs = $this->uri->segment_array();
        $segs[1] = $this->coutry_iso;
        // echo $new_uri = str_replace("/".$this->uri->segment(1), $this->coutry_iso."/", uri_string());
        redirect(base_url($segs), 'refresh');
      }

    }






    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
      $this->coutry_iso = "";
    }

    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    $data['noindex'] = true;
    $data['page'] = 'forgot_password/forgot_password_v';
    $this->load->view('front_template', $data);
  }

  public function request_new_password($code){
    $data['noindex'] = true;
    $identity_column = $this->config->item('identity', 'ion_auth');
    if($this->input->post('register_by') == 'email'){
      $identity = $this->ion_auth->where($identity_column, $this->input->post('email'))->users()->row();
    }
    else{
      $identity = $this->ion_auth->where($identity_column, $this->input->post('phone'))->users()->row();
    }


    if (empty($identity)){


    }
    else{

    }

    // print_r($identity);

    // run the forgotten password method to email an activation code to the user
    $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});


    // print_r($forgotten);

    if ($forgotten)
    {
      // if there were no errors
      // $this->session->set_flashdata('message', $this->ion_auth->messages());
      // redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
      if($this->input->post('register_by') == 'email'){
        $msg = 'Reset your new password <a href="'.base_url("page/forgot_password/new_password/").$forgotten["forgotten_password_code"].'">Click</a>';
        mail_to(array($this->input->post('email')=>''), "Reset new password", $msg);


      }
      else{
        $text = 'Reset your new password '.base_url("page/forgot_password/new_password/").$forgotten["forgotten_password_code"];
        require_once 'vendor/autoload.php';
        $basic  = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
        $client = new \Nexmo\Client($basic);

        $this->db->where('nicename', $identity->country);
        $countryCode = $this->datacontrol_model->getRowData('countries');
        $phone = $identity->phone;
        // $phone = '0803951444';
        // $phone = '0823251212';
        $firstCharacter = substr($phone, 0, 1);
        if($firstCharacter == 0){
          $phone = substr($phone, 1);
        }

        $message = $client->message()->send([
          'to' => '+'.$countryCode->phonecode.$phone,
          'from' => 'Sensha',
          'text' => $text,
          'type' => 'unicode'
        ]);


      }

      $data['page'] = 'forgot_password/email_send_v';
      $this->load->view('front_template', $data);
    }
    else
    {
      // $this->session->set_flashdata('message', $this->ion_auth->errors());
      redirect("auth/forgot_password", 'refresh');
    }


  }

  public function new_password($code){
    $data['noindex'] = true;
    //
    if (!$code)
		{
			$this->output->set_status_header('403');
		}

    $user = $this->ion_auth->forgotten_password_check($code);

    if ($user){
      $data['page'] = 'forgot_password/new_password_v';
      $data['code'] = $code;
      $this->load->view('front_template', $data);
    }
    else{
      // $this->output->set_status_header('403');
      redirect('page/forgot_password', 'refresh');
    }

  }

  public function reset_password(){
    $data['noindex'] = true;
    // finally change the password
    $user = $this->ion_auth->forgotten_password_check($this->input->post('code'));
    if($user){
      $identity = $user->{$this->config->item('identity', 'ion_auth')};

      $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

      if ($change)
      {
        // if the password was successfully changed
        redirect('page/forgot_password/reset_password_success', 'refresh');
      }
      else
      {
        redirect('page/forgot_password/new_password/'.$this->input->post('code'), 'refresh');
      }
    }
      redirect('page/forgot_password/new_password/'.$this->input->post('code'), 'refresh');
  }

  public function reset_password_success(){
    $data['noindex'] = true;
    $data['page'] = 'forgot_password/success_v';
    $this->load->view('front_template', $data);
  }


}
