<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sensha Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/font-awesome/css/font-awesome.min.css") ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.min.css") ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/dist/css/adminlte.min.css") ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery.min.js") ?>"></script>
    <!-- sweetalert2 -->
    <script src="<?= base_url("node_modules/sweetalert2/dist/sweetalert2.min.js") ?>"></script>
    <link rel="stylesheet" href="<?= base_url("node_modules/sweetalert2/dist/sweetalert2.min.css") ?>">
    <!--  custom css  -->
    <link rel="stylesheet" href="<?=base_url("assets/css/custom.css")?>">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom d-lg-none">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link">
            <!-- <img src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/dist/img/AdminLTELogo.png") ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
            <?php
            $account = '';
            if ($this->ion_auth->in_group(array('admin'))) {
                $account = 'Admin';
            }
            if ($this->ion_auth->in_group(array('SA'))) {
                $account = 'SA';
            }
            if ($this->ion_auth->in_group(array('AA'))) {
                $account = 'AA';
            }
            ?>
            <span class="brand-text font-weight-light">Sensha <?php echo $account; ?></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">


            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA', 'AA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/dashboard") ?>"
                               class="nav-link <?php echo ($menu == 'dashboard') ? "active" : "" ?>">
                                <i class="fa fa-dashboard nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_dashboard_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA', 'AA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/financial_manager") ?>"
                               class="nav-link <?php echo ($menu == 'financial_manager') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_financial_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/nation_lang") ?>"
                               class="nav-link <?php echo ($menu == 'nation_lang') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_nation_lang_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/account_setting") ?>"
                               class="nav-link <?php echo ($menu == 'account_setting') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_account_setting_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA', 'AA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/sales_history") ?>"
                               class="nav-link <?php echo ($menu == 'sales_history') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/cluster1") ?>"
                               class="nav-link <?php echo ($menu == 'product_category') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_product_cat_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <!-- <li class="nav-item">
            <a href="<?php echo base_url("sensha-admin/cluster1") ?>" class="nav-link <?php echo ($menu == 'cluster1') ? "active" : "" ?>">
              <i class="fa fa-circle-o nav-icon"></i>
              <p><?php echo $this->lang->line('admin_left_menu-cluster1', FALSE); ?></p>
            </a>
          </li> -->

                        <!-- <li class="nav-item">
            <a href="<?php echo base_url("sensha-admin/cluster2") ?>" class="nav-link <?php echo ($menu == 'cluster2') ? "active" : "" ?>">
              <i class="fa fa-circle-o nav-icon"></i>
              <p><?php echo $this->lang->line('admin_left_menu-cluster2', FALSE); ?></p>
            </a>
          </li> -->

                        <!-- <li class="nav-item">
            <a href="<?php echo base_url("sensha-admin/cluster3") ?>" class="nav-link <?php echo ($menu == 'cluster3') ? "active" : "" ?>">
              <i class="fa fa-circle-o nav-icon"></i>
              <p><?php echo $this->lang->line('admin_left_menu-cluster3', FALSE); ?></p>
            </a>
          </li> -->
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/product_management") ?>"
                               class="nav-link <?php echo ($menu == 'product_management') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_product_management_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/delivery") ?>"
                               class="nav-link <?php echo ($menu == 'delivery') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_delivery_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php /* if($this->ion_auth->in_group(array('admin'))):?>
          <li class="nav-item">
            <a href="<?php echo base_url("sensha-admin/exclude_group")?>" class="nav-link <?php echo ($menu == 'exclude_group')?"active":""?>">
              <i class="fa fa-circle-o nav-icon"></i>
              <p><?php echo $this->lang->line('admin_exclude_title', FALSE); ?></p>
            </a>
          </li>
        <?php endif; */ ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/discount_coupon") ?>"
                               class="nav-link <?php echo ($menu == 'discount_coupon') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_discount_coupon_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/webpage_setting") ?>"
                               class="nav-link <?php echo ($menu == 'webpage_setting') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_webpage_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/news_post") ?>"
                               class="nav-link <?php echo ($menu == 'news_post') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_post_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin', 'SA'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/qa_post") ?>"
                               class="nav-link <?php echo ($menu == 'qa_post') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_qa_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/global_network") ?>"
                               class="nav-link <?php echo ($menu == 'global_network') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo $this->lang->line('admin_gn_title', FALSE); ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <!-- <li class="nav-header">MEMBERS</li> -->
                    <!-- <li class="nav-item">
        <a href="<?php echo base_url("sensha-admin/usersList"); ?>" class="nav-link <?php echo ($menu == 'usersList') ? "active" : "" ?>">
        <i class="fa fa-circle-o nav-icon"></i>
        <p>Members List</p>
      </a>
    </li> -->
                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <!-- <li class="nav-item">
        <a href="<?php echo base_url("sensha-admin/usersGroups") ?>" class="nav-link <?php echo ($menu == 'usersGroups') ? "active" : "" ?>">
          <i class="fa fa-circle-o nav-icon"></i>
          <p>Members Group</p>
        </a>
      </li> -->
                    <?php endif; ?>

                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url("sensha-admin/login_setting") ?>"
                               class="nav-link <?php echo ($menu == 'login_setting') ? "active" : "" ?>">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p><?php echo "Setting Active login" ?></p>
                            </a>
                        </li>
                    <?php endif; ?>

                    <li class="nav-item">
                        <a href="<?php echo base_url("auth/logout") ?>" class="nav-link">
                            <i class="fa fa-sign-out nav-icon"></i>
                            <p>Logout</p>
                        </a>
                    </li>

                    <!-- <li class="nav-header">MISCELLANEOUS</li>
                    <li class="nav-item">
                    <a href="https://adminlte.io/docs" class="nav-link">
                    <i class="nav-icon fa fa-file"></i>
                    <p>Documentation</p>
                  </a>
                </li> -->
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php $this->load->view($content_view); ?>
    </div>

    <footer class="main-footer">
        <strong>Copyright &copy; <?php echo date("Y"); ?></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <!-- <b>Version</b> 3.0.0-alpha -->
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/slimScroll/jquery.slimscroll.min.js") ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/fastclick/fastclick.js") ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/dist/js/adminlte.js") ?>"></script>

</body>
</html>
