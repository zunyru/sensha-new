<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Users List</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Users List</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success btn-add pull-right"><i class="fa fa-plus"></i> Add</button>
        <br><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <!-- <div class="card-header">
          <h3 class="card-title">Hover Data Table</h3>
        </div> -->
        <div class="card-body">
          <table class="table table-bordered table-hover no-margin" id="list_table">
            <thead>
              <tr>
                <th width="5%">#</th>
                <th>Name</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Users Group</th>
                <th width="10%"></th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1; foreach($users_list as $item):?>
                <tr>
                  <td><?=$n?></td>
                  <td><?=$item->first_name?></td>
                  <td><?=$item->last_name?></td>
                  <td><?=$item->email?></td>
                  <td>
                    <?php
                    $my_group = array();
                    foreach($item->groups as $group): array_push($my_group, $group->name);
                    ?>
                    <?=$group->name?> <br />
                  <?php endforeach;?>
                </td>
                <td>
                  <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?=$item->id?>" data-firstname="<?=$item->first_name?>" data-lastname="<?=$item->last_name?>" data-email="<?=$item->email?>" data-usergroup="<?=implode(',', $my_group)?>">
                    <i class="fa fa-pencil-square-o"></i>
                  </button>
                  <!-- <button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" data-id="<?=$item->id?>" data-firstname="<?=$item->first_name?>" data-lastname="<?=$item->last_name?>" data-email="<?=$item->email?>">
                    <i class="fa fa-trash-o"></i>
                  </button> -->
                </td>
              </tr>
              <?php $n++; endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="modal-add" class="modal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_add" method="post" enctype="multipart/form-data" action="<?=base_url("auth/createUser")?>">
          <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Email" required="yes">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="first_name" placeholder="First Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
          </div>
          <div class="form-group">
            <label>User Group</label>

            <div class="checkbox">
              <?php foreach($groups_list as $item):?>
                <label style="margin-right: 15px;"><input type="checkbox" name="groups[]" value="<?=$item->id?>" /> <?=$item->name?></label>
              <?php endforeach;?>
            </div>

          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


<div id="modal-edit" class="modal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_edit" method="post" enctype="multipart/form-data" action="<?=base_url("auth/edit_user")?>">
            <input type="hidden" id="edit_id" name="edit_id" />
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" id="email" disabled="true" name="email" placeholder="Email" required="yes">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            <div class="form-group">
              <label>First Name</label>
              <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
            </div>
            <div class="form-group">
              <label>User Group</label>

              <div class="checkbox">
                <?php foreach($groups_list as $item):?>
                  <label style="margin-right: 15px;"><input type="checkbox" data-name="<?=$item->name?>" name="groups[]" value="<?=$item->id?>"> <?=$item->name?></label>
                <?php endforeach;?>
              </div>

            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>


<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
<script src="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
<script src="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>

<script>

  function edit_item(t){
    $('#modal-edit #email').val($(t).data('email'));
    $('#modal-edit #first_name').val($(t).data('firstname'));
    $('#modal-edit #last_name').val($(t).data('lastname'));
    var user_group = $(t).data('usergroup').split(",");
    $("#modal-edit input[type='checkbox']").prop('checked', false);
    for(i=0; i < user_group.length; i++){
      $("#modal-edit input[type='checkbox']").each(function(){
        if($(this).data('name') == user_group[i]){
          $(this).prop('checked', true);
        }
      });
    }
    $('#edit_id').val($(t).data('id'));
    $('#modal-edit').modal();
  }

  function delete_item(t){
    Swal.fire({
      title: 'Are you sure to delete <br /> "'+$(t).data('firstname')+' '+$(t).data('lastname')+'"?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          method: "POST",
          url: '<?=base_url("sensha-admin/userDeactivate/")?>'+$(t).data('id'),
        })
        .done(function( msg ) {
          // r = JSON.parse(msg);
          Swal.fire({
            title: 'Deleted!',
            text: 'Your data has been deleted.',
            type: 'success',
            showConfirmButton: false,
          })
          setTimeout(function(){ window.location = ''; }, 1400);
        })
        .fail(function( msg ) {
          Swal.fire({
            type: 'error',
            // text: msg.responseText,
            title: msg.statusText,
            // showConfirmButton: false
          });
        });
      }
    });
  }

  $(function(){
    $('#list_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "pageLength": 30
    });

    $('.btn-add').click(function(){
      $('#modal-add').modal();
    });

    $('#form_add').submit(function(){
      $.ajax({
        method: "POST",
        url: $(this).attr('action'),
        data: $( this ).serialize()
      })
      .done(function( msg ) {
        // console.log(msg);
        r = JSON.parse(msg);
        console.log(r);
        if(r.error == 0){
          Swal.fire({
            type: 'success',
            title: 'Add data complete',
            showConfirmButton: false,
            allowOutsideClick:false
          });
          setTimeout(function(){ location.reload(); }, 1400);
        }
        else{
          Swal.fire(
            'Oops...',
            'Something went wrong!',
            'error'
          );
        }
      })
      .fail(function( msg ) {
        Swal.fire(
          'Oops...',
          msg,
          'error'
        );
      });
      return false;
    });

    $('#form_edit').submit(function(){
      $.ajax({
        method: "POST",
        url: $(this).attr('action'),
        data: $( this ).serialize()
      })
      .done(function( msg ) {
        // console.log(msg);
        r = JSON.parse(msg);
        console.log(r);
        if(r.error == 0){
          Swal.fire({
            type: 'success',
            title: 'Update data complete',
            showConfirmButton: false,
            allowOutsideClick:false
          });
          setTimeout(function(){ location.reload(); }, 1400);
        }
        else{
          Swal.fire(
            'Oops...',
            'Something went wrong!',
            'error'
          );
        }
      })
      .fail(function( msg ) {
        Swal.fire(
          'Oops...',
          msg,
          'error'
        );
      });
      return false;
    });

  });

</script>
