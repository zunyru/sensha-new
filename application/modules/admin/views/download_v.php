<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark">Download File</h1>
            </div>
            <div class="col-sm-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">CSV File</h3>
                    </div>
                    <div class="card-body">
                        <ul>
                            <?php foreach ($files['files_csv'] as $newFileName) { ?>
                            <li style="margin-top: 5px;">
                                <?php echo str_replace("uploads/export/","",$newFileName) ?> <a href="<?php echo base_url().$newFileName?>" class="btn btn-primary">Download</a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>