<style>
  /* .modal-lg{
width: 80%;
} */
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_discount_coupon_title', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_discount_coupon_title', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <?php if ($this->ion_auth->in_group(array('admin'))) : ?>
      <div class="row">
        <div class="col-md-4">
          <form method="post" action="<?php echo base_url("sensha-admin/discount_coupon"); ?>">
            <div class="form-group">
              <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
              <select name="country">
                <option value="">--- Choose Country ---</option>
                <?php foreach ($nation_lang as $item) : ?>
                  <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_"," ", $item->country)) ?></option>
                <?php endforeach; ?>
              </select>
              <button type="submit" class="btn btn-primary btn-sm"><?php echo $this->lang->line('global_search', FALSE); ?></button>
            </div>
          </form>
        </div>
      </div>
    <?php endif; ?>


    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
        <br><br>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?php echo $this->lang->line('admin_discount_coupont_title', FALSE); ?></h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover no-margin" id="list_table">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th><?php echo $this->lang->line('admin_nation_lang_country', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_discount_coupon_coupon_name', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_discount_coupon_coupon_code', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_discount_coupon_discount_type', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_discount_coupon_discount', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_discount_coupon_start_date', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_discount_coupon_end_date', FALSE); ?></th>
                  <th width="10%"></th>
                </tr>
              </thead>
              <tbody>
                <?php if (!empty($discount_coupon)) : ?>
                  <?php $n = 1;
                  foreach ($discount_coupon as $item) : ?>
                    <tr>
                      <td><?php echo $n; ?></td>
                      <td><?php echo ucwords(str_replace("_"," ", $item->country)) ?></td>
                      <td><?php echo $item->coupon_name; ?></td>
                      <td><?php echo $item->coupon_code; ?></td>
                      <td><?php echo $item->discount_type; ?></td>
                      <td><?php echo $item->discount; ?></td>
                      <td><?php echo $item->start_date; ?></td>
                      <td><?php echo $item->end_date; ?></td>
                      <td>
                        <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?php echo $item->id ?>" data-country="<?php echo $item->country ?>" data-coupon_name="<?php echo $item->coupon_name ?>" data-coupon_code="<?php echo $item->coupon_code ?>" data-discount_type="<?php echo $item->discount_type ?>" data-discount="<?php echo $item->discount ?>" data-start_date="<?php echo $item->start_date ?>" data-end_date="<?php echo $item->end_date ?>"><i class="fa fa-pencil-square-o"></i></button>
                        <button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" data-id="<?php echo $item->id ?>" data-coupon_name="<?php echo $item->coupon_name ?>"><i class="fa fa-trash-o"></i></button>
                      </td>
                    </tr>
                  <?php $n++;
                  endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div id="modal-add" class="modal" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo $this->lang->line('admin_discount_coupon_create_discount_coupon', FALSE); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/discount_coupon/add"); ?>">
              <div class="card-body">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_nation_lang_country', FALSE); ?></label>
                  <select class="form-control select2" name="country" style="width: 100%;">
                    <option value="">-- Choose --</option>
                    <?php foreach ($nation_lang as $item) : ?>
                      <?php if ($this->ion_auth->in_group(array('SA'))) { ?>
                        <?php if ($this_country == $item->country) { ?>
                          <option value="<?php echo $item->country; ?>" selected="selected"><?php echo ucwords(str_replace("_"," ", $item->country)) ?></option>
                        <?php } ?>
                      <?php } else { ?>
                        <option value="<?php echo $item->country; ?>" selected="selected"><?php echo ucwords(str_replace("_"," ", $item->country)) ?></option>
                      <?php } ?>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_discount_coupon_coupon_name', FALSE); ?></label>
                  <input type="text" class="form-control" name="coupon_name">
                </div>
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_discount_coupon_coupon_code', FALSE); ?></label>
                  <input type="text" class="form-control" name="coupon_code">
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_discount_type', FALSE); ?></label>
                      <select class="form-control select2" name="discount_type" style="width: 100%;">
                        <option selected="selected" value="">-- Choose --</option>
                        <option value="percent">Percent</option>
                      </select>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_discount', FALSE); ?></label>
                      <input class="form-control" name="discount" max="20" type="number">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_start_date', FALSE); ?></label>
                      <input type="text" class="form-control datepicker" name="start_date">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_end_date', FALSE); ?></label>
                      <input type="text" class="form-control datepicker" name="end_date">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div id="modal-edit" class="modal" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo $this->lang->line('admin_discount_coupon_edit_discount_coupon', FALSE); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/discount_coupon/edit"); ?>">
              <input type="hidden" id="edit_id" name="edit_id" />
              <div class="card-body">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_nation_lang_country', FALSE); ?></label>
                  <select class="form-control select2" name="country" style="width: 100%;">
                    <option selected="selected" value="">-- Choose --</option>
                    <?php foreach ($nation_lang as $item) : ?>
                      <?php if ($this->ion_auth->in_group(array('SA'))) { ?>
                        <?php if ($this_country == $item->country) { ?>
                          <option value="<?php echo $item->country; ?>" selected="selected"><?php echo ucwords(str_replace("_"," ", $item->country)) ?></option>
                        <?php } ?>
                      <?php } else { ?>
                        <option value="<?php echo $item->country; ?>" selected="selected"><?php echo ucwords(str_replace("_"," ", $item->country)) ?></option>
                      <?php } ?>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_discount_coupon_coupon_name', FALSE); ?></label>
                  <input type="text" class="form-control" name="coupon_name">
                </div>
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_discount_coupon_coupon_code', FALSE); ?></label>
                  <input type="text" class="form-control" name="coupon_code">
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_discount_type', FALSE); ?></label>
                      <select class="form-control select2" name="discount_type" style="width: 100%;">
                        <option selected="selected" value="">-- Choose --</option>
                        <option value="percent">Percent</option>
                      </select>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_discount', FALSE); ?></label>
                      <input  class="form-control" name="discount" max="20" type="number">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_start_date', FALSE); ?></label>
                      <input type="text" class="form-control datepicker" name="start_date">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_discount_coupon_end_date', FALSE); ?></label>
                      <input type="text" class="form-control datepicker" name="end_date">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

</section>


<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?>"></script>
<!-- Date picker -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/datetimepicker-master/jquery.datetimepicker.css") ?>" />
<script src="<?php echo base_url("assets/datetimepicker-master/build/jquery.datetimepicker.full.min.js") ?>"></script>

<script>
  function edit_item(t) {
    $('#modal-edit select[name="country"]').select2("val", $(t).data('country'));

    $('#modal-edit input[name="coupon_name"]').val($(t).data('coupon_name'));
    $('#modal-edit input[name="coupon_code"]').val($(t).data('coupon_code'));
    $('#modal-edit select[name="discount_type"]').select2("val", $(t).data('discount_type'));
    $('#modal-edit input[name="discount"]').val($(t).data('discount'));
    $('#modal-edit input[name="start_date"]').val($(t).data('start_date'));
    $('#modal-edit input[name="end_date"]').val($(t).data('end_date'));

    $('#edit_id').val($(t).data('id'));
    $('#modal-edit').modal();
  }

  function delete_item(t) {
    Swal.fire({
      title: 'Are you sure to delete <br /> "' + $(t).data('coupon_name') + '"?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            method: "POST",
            url: '<?= base_url("sensha-admin/discount_coupon/delete/") ?>' + $(t).data('id'),
          })
          .done(function(msg) {
            // r = JSON.parse(msg);
            Swal.fire({
              title: 'Deleted!',
              text: 'Your data has been deleted.',
              type: 'success',
              showConfirmButton: false,
            })
            setTimeout(function() {
              window.location = '';
            }, 1400);
          })
          .fail(function(msg) {
            Swal.fire({
              type: 'error',
              // text: msg.responseText,
              title: msg.statusText,
              // showConfirmButton: false
            });
          });
      }
    });
  }

  $(function() {
    $('#list_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "pageLength": 30
    });

    $('.datepicker').datetimepicker({
      timepicker: false,
      format: 'Y-m-d',
    });

    $('.select2').select2();

    $('#form_add, #form_edit').submit(function() {
      $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $(this).serialize()
        })
        .done(function(msg) {
          // console.log(msg);
          r = JSON.parse(msg);
          // console.log(r);
          if (r.error == 0) {
            Swal.fire({
              type: 'success',
              title: 'success',
              showConfirmButton: false,
              allowOutsideClick: false
            });
            setTimeout(function() {
              location.reload();
            }, 1400);
          } else if (r.error == 2) {
            Swal.fire({
              type: 'warning',
              title: 'Coupon code is duplicate.',
              showConfirmButton: true,
              allowOutsideClick: false
            });
          } else {
            Swal.fire(
              'Oops...',
              'Something went wrong!',
              'error'
            );
          }
        })
        .fail(function(msg) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
      return false;
    });
  });
</script>