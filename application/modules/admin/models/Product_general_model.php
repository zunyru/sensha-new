<?php
class Product_general_model extends CI_Model
{
    public function get_rows($param)
    {
        $user_country = $this->ion_auth->user()->row()->country;

        $this->_condition($param);
        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        if($this->ion_auth->in_group(array('SA')))
            $this->db->where('product_country', $user_country);

        if(!empty($param['filter_country']))
            $this->db->where('product_country', $this->input->post('filter_country'));

        $query = $this->db
            ->select('*')
            ->from('product_general')
            ->order_by('product_id', 'asc')
            ->get();

        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {

        $user_country = $this->ion_auth->user()->row()->country;

        $this->_condition($param);
        if($this->ion_auth->in_group(array('SA')))
            $this->db->where('product_country', $user_country);

        if(!empty($param['filter_country']))
            $this->db->where('product_country', $this->input->post('filter_country'));

        $query = $this->db
            ->select('*')
            ->from('product_general')
            ->order_by('product_id', 'asc')
            ->get();

        return $query->num_rows();
    }

    private function _condition($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('product_id', $param['search']['value'])
                ->or_like('product_name', $param['search']['value'])
                ->group_end();
        }
    }
}