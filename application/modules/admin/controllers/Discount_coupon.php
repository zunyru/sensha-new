<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_coupon extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    $user_country = $this->ion_auth->user()->row()->country;
    
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    if($this->input->post('country')){
      $this->db->where('country', $this->input->post('country'));
    }
    if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('country', $user_country);
    }
    $data['discount_coupon'] = $this->datacontrol_model->getAllData('discount_coupon');

    $data["content_view"] = 'admin/discount_coupon/discount_coupon_v';
    $data["menu"] = 'discount_coupon';
    $data["htmlTitle"] = "Discount Coupon";
	$data['this_country'] = $user_country;

    $this->load->view('admin_template', $data);
  }

  public function add(){
    $this->db->where('coupon_code', $this->input->post('coupon_code', TRUE));
    $getCode = $this->datacontrol_model->getRowData('discount_coupon');
    if(!empty($getCode)){
      echo json_encode(array('error' => 2));
    }
    else{
      $data = array(
        'country' => $this->input->post('country', TRUE),
        'coupon_name' => $this->input->post('coupon_name', TRUE),
        'coupon_code' => $this->input->post('coupon_code', TRUE),
        'discount_type' => $this->input->post('discount_type', TRUE),
        'discount' => $this->input->post('discount', TRUE),
        'start_date' => $this->input->post('start_date', TRUE),
        'end_date' => $this->input->post('end_date', TRUE),
      );
      $affected_rows = $this->datacontrol_model->insert('discount_coupon', $data);
      if ($affected_rows > 0) {
        echo json_encode(array('error' => 0));
      } else {
        echo json_encode(array('error' => 1));
      }
    }
    
  }

  public function edit(){
    $data = array(
      'country' => $this->input->post('country', TRUE),
      'coupon_name' => $this->input->post('coupon_name', TRUE),
      'coupon_code' => $this->input->post('coupon_code', TRUE),
      'discount_type' => $this->input->post('discount_type', TRUE),
      'discount' => $this->input->post('discount', TRUE),
      'start_date' => $this->input->post('start_date', TRUE),
      'end_date' => $this->input->post('end_date', TRUE),
    );

    $affected_rows = $this->datacontrol_model->update('discount_coupon', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete($id){
    $this->datacontrol_model->delete('discount_coupon', array('id'=>$id));
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
