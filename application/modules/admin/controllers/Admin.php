<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index()
  {
    // $this->load->model('example_model');
    // $this->example_model->xx();

    // $this->load->model('database/datacontrol_model');
    // $this->datacontrol_model->test();

    $data["content_view"] = 'admin/dashboard_v';
    $data["menu"] = 'dashboard';
    $data["htmlTitle"] = "Dashboard";

    // $this->load->view('admin_template', $data);
  }

  public function usersList(){
    $data["content_view"] = 'admin/users/usersList_v';
    $data["menu"] = 'usersList';
    $data["htmlTitle"] = "Users List";

    $this->data['users'] = $this->ion_auth->users()->result();
    foreach($this->data['users'] as $k => $user){
      $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
    }

    $data['users_list'] = $this->data['users'];

    $query = $this->db->get('groups');
    $data['groups_list'] = $query->result();

    $this->load->view('admin_template', $data);
  }

  public function usersGroups(){
    $data["content_view"] = 'admin/users/usersGroups_v';
    $data["menu"] = 'usersGroups';
    $data["htmlTitle"] = "Users Group";

    $query = $this->db->get('groups');
    $data['groups_list'] = $query->result();

    $this->load->view('admin_template', $data);
  }

  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }

}
