<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_network extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in()){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){

    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    $data['global_network'] = $this->datacontrol_model->getAllData('global_network');

    $data["content_view"] = 'admin/global_network/global_network_v';
    $data["menu"] = 'global_network';
    $data["htmlTitle"] = "Global network";

    $this->load->view('admin_template', $data);
  }

  public function add(){
    $data = array(
      'country' => $this->input->post('country', TRUE),
      'company' => $this->input->post('company', TRUE),
      'addr' => $this->input->post('addr', TRUE),
      'phone' => $this->input->post('phone', TRUE),
      'latitude' => $this->input->post('latitude', TRUE),
      'longitude' => $this->input->post('longitude', TRUE),
    );
    $affected_rows = $this->datacontrol_model->insert('global_network', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
    $data = array(
      'country' => $this->input->post('country', TRUE),
      'company' => $this->input->post('company', TRUE),
      'addr' => $this->input->post('addr', TRUE),
      'phone' => $this->input->post('phone', TRUE),
      'latitude' => $this->input->post('latitude', TRUE),
      'longitude' => $this->input->post('longitude', TRUE),
    );
    $image = $this->upload_branch_image();
    if($image['branch_image'] != ''){
      $data['branch_image'] = $image['branch_image'];
    }
    $affected_rows = $this->datacontrol_model->update('global_network', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete($id){
    $affected_rows = $this->datacontrol_model->delete('global_network', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  function upload_branch_image(){
    $branch_image = '';
    if (!file_exists('uploads/global_network/')) {
      mkdir("uploads/global_network/", 0777);
    }

    if(!empty($_FILES['branch_image']['name'])){

      $config['upload_path']          = 'uploads/global_network/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('branch_image'))
      {
        // echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        $branch_image = $data['file_name'];
        // echo $data['file_name'];
        // echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }


    return array('branch_image' => $branch_image);
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
