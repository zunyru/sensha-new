<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sensha Admin | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/font-awesome/css/font-awesome.min.css")?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/dist/css/adminlte.min.css") ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/iCheck/square/blue.css") ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- jQuery -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery.min.js") ?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/iCheck/icheck.min.js") ?>"></script>

  <!-- sweetalert2 -->
  <script src="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.all.min.js")?>"></script>
  <link rel="stylesheet" href="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.css")?>">
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Sensha</b> Admin</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?php
        if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) {
            echo "<p style='color: red'>Lock !!!</p>";
        }
        ?>
        <form id="formUserLogin" action="<?=base_url("auth/dologin")?>" method="post">
          <div class="form-group has-feedback">
            <input type="email" class="form-control" id="CustomerEmail" placeholder="Email" name="email" required="yes"
                   <?php if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) { echo 'style="background-color: #ccc"'; echo ' disabled'; }?>>
            <!-- <span class="fa fa-envelope form-control-feedback"></span> -->
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" id="CustomerPassword" placeholder="Password" name="pass" required="yes"
                <?php if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) { echo 'style="background-color: #ccc"'; echo ' disabled'; }?>>
            <!-- <span class="fa fa-lock form-control-feedback"></span> -->
          </div>
          <div class="row">
            <div class="col-8">
              <!-- <div class="checkbox icheck">
                <label>
                  <input type="checkbox" name="remember" value="1"> Remember Me
                </label>
              </div> -->
            </div>
            <!-- /.col -->
            <div class="col-4">
              <button type="submit"  class="btn btn-primary btn-block btn-flat sign-in-btn" <?php if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) { echo 'style="background-color: #ccc"'; echo ' disabled'; }?>>Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <!-- <div class="social-auth-links text-center mb-3">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-primary">
            <i class="fa fa-facebook mr-2"></i> Sign in using Facebook
          </a>
          <a href="#" class="btn btn-block btn-danger">
            <i class="fa fa-google-plus mr-2"></i> Sign in using Google+
          </a>
        </div> -->
        <!-- /.social-auth-links -->

        <p class="mb-1">
          <a href="#" id="lost-pass">I forgot my password</a>
        </p>
        <p class="mb-0">
          <!-- <a href="<?php echo base_url("auth/register")?>" class="text-center">Register a new membership</a> -->
        </p>
      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->


  <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    });

    $("#formUserLogin").submit(function(){
      Swal.fire({
        title: 'Processing',
        allowOutsideClick:false,
        onBeforeOpen: () => {
          swal.showLoading();
        }
      });

      $.ajax({
        method: "POST",
        url: $(this).attr('action'),
        data: $( this ).serialize()
      })
      .done(function( msg ) {
        r = JSON.parse(msg);
        if(r.error == 0){
          Swal.fire({
            type: 'success',
            title: 'Login Success',
            showConfirmButton: false,
            allowOutsideClick:false
          });
          setTimeout(function(){ window.location = '<?=base_url("sensha-admin/dashboard")?>'; }, 1500);
        }
        else if(r.error == 1){
          Swal.fire({
            type: 'error',
            title: 'Please verify your email',
          });
        }
        if(r.error == 2){
            var lock = r.lock;
            if(lock > 5){
                $('#CustomerEmail').prop("disabled", true);
                $('#CustomerPassword').prop("disabled", true);
                $('.sign-in-btn').prop("disabled", true);
                $("#CustomerEmail").css("background-color","#ccc");
                $("#CustomerPassword").css("background-color","#ccc");
                $('.sign-in-btn').css("background","#6c757d");

            }
          Swal.fire({
            type: 'error',
            title: r.msg,
          });
        }

        if(r.error == 3){
            var lock = r.lock;
            if(lock > 5){
                $('#CustomerEmail').prop("disabled", true);
                $('#CustomerPassword').prop("disabled", true);
                $('.sign-in-btn').prop("disabled", true);
                $("#CustomerEmail").css("background-color","#ccc");
                $("#CustomerPassword").css("background-color","#ccc");
                $('.sign-in-btn').css("background","#6c757d");

            }
          Swal.fire({
            type: 'error',
            title: 'Not found you account.',
          });

        }
      })
      .fail(function( msg ) {
        Swal.fire(
          'Oops...',
          msg,
          'error'
        );
      });

      return false;
    });

    $('#lost-pass').click(function(){
      Swal.fire({
        title: 'Input your email.',
        input: 'email',
        showCancelButton: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
        preConfirm: (email) => {
          return new Promise((resolve) => {
            $.ajax({
              method: "POST",
              url: '<?=base_url("auth/forgot_password")?>',
              data: {'email': email}
            })
            .done(function( msg ) {
              console.log(msg);
              r = JSON.parse(msg);
              // console.log(r);

              if(r.error == 0){
                resolve()
              }
              else{
                Swal.fire(
                  'Oops...',
                  r.msg,
                  'error'
                );
              }

            })
            .fail(function( msg ) {
              Swal.fire(
                'Oops...',
                msg,
                'error'
              );
            });
            // resolve()
          })
        },
        // allowOutsideClick: () => !Swal.fire.isLoading()
      }).then((result) => {
        if (result.value) {
          Swal.fire({
            type: 'success',
            title: 'Request change password finished!',
            html: 'email send to: ' + result.value +'<br /> Please check your email.',
            allowOutsideClick: false,
          })
        }
      })
    });

  });
</script>
</body>
</html>
