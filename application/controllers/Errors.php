<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {
	public function custom404() {
		// リクエストされたURIが画像や特定のリソースの場合は、エラーログに記録しない
		$uri = $_SERVER['REQUEST_URI'];
		if (preg_match('/\.(jpg|jpeg|png|gif|css|js)$/', $uri)) {
			// ログに記録しない処理
			return;
		}

		// 通常の404エラーハンドリング (既存の404ページを読み込む)
		$this->load->view('404');  // application/views/404.phpを表示
	}
}