class com.jeroenwijering.players.CaptionsView extends com.jeroenwijering.players.AbstractView
{
    var parser, parseArray, config, clip, captions, feeder, currentTime, capTime;
    function CaptionsView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        var ref = this;
        Stage.addListener(this);
        parser = new com.jeroenwijering.players.CaptionsParser();
        parser.onParseComplete = function ()
        {
            parseArray.sortOn("bgn", Array.NUMERIC);
            ref.captions = parseArray;
            false;
        };
        clip = config.clip.captions;
        this.setDimensions();
    } // End of the function
    function setDimensions()
    {
        clip.txt.autoSize = "center";
        clip.bck._height = clip.txt._height + 10;
        if (Stage.displayState == "fullScreen")
        {
            clip._width = Stage.width;
            clip._yscale = clip._xscale;
            clip._y = Stage.height - clip._height;
        }
        else
        {
            clip._width = config.displaywidth;
            clip._yscale = clip._xscale;
            clip._y = config.displayheight - clip._height;
        } // end else if
        if (System.capabilities.version.indexOf("7,0,") == -1)
        {
            var _loc2 = 2 + Math.round(clip._yscale / 100);
            var _loc3 = new flash.filters.DropShadowFilter(0, 0, 0, 1, _loc2, _loc2, 50, 2);
            clip.filters = new Array(_loc3);
        } // end if
    } // End of the function
    function setItem(idx)
    {
        captions = new Array();
        if (feeder.feed[idx].captions == undefined)
        {
            clip.bck._alpha = 0;
        }
        else if (feeder.feed[idx].captions.indexOf("captionate") > -1 || feeder.feed[idx].captions == "true")
        {
            captionate = true;
            var _loc3 = Number(feeder.feed[idx].captions.substr(-1));
            if (isNaN(_loc3))
            {
                capTrack = 0;
            }
            else
            {
                capTrack = _loc3;
            } // end else if
        }
        else
        {
            parser.parse(feeder.feed[idx].captions);
        } // end else if
    } // End of the function
    function setTime(elp, rem)
    {
        currentTime = elp;
        if (captionate == false)
        {
            this.setCaption();
        } // end if
    } // End of the function
    function setCaption()
    {
        var _loc3 = captions.length;
        for (var _loc2 = 0; _loc2 < captions.length; ++_loc2)
        {
            if (captions[_loc2].bgn > currentTime)
            {
                _loc3 = _loc2;
                break;
            } // end if
        } // end of for
        if (captions[_loc3 - 1].bgn + captions[_loc3 - 1].dur > currentTime)
        {
            clip.txt.htmlText = captions[_loc3 - 1].txt;
            if (System.capabilities.version.indexOf("7,0,") > -1)
            {
                clip.bck._alpha = 50;
                clip.bck._height = Math.round(clip.txt._height + 10);
            }
            else
            {
                clip.bck._height = Math.round(clip.txt._height + 15);
            } // end else if
            if (Stage.displayState == "fullScreen")
            {
                clip._y = Stage.height - clip._height;
            }
            else
            {
                clip._y = config.displayheight - clip._height;
            } // end else if
        }
        else
        {
            clip.txt.htmlText = "";
        } // end else if
    } // End of the function
    function onCaptionate(cap)
    {
        clip.txt.htmlText = cap[capTrack];
        capTime = currentTime;
    } // End of the function
    function onResize()
    {
        this.setDimensions();
    } // End of the function
    function onFullScreen(fs)
    {
        if (fs == false)
        {
            this.setDimensions();
        } // end if
    } // End of the function
    var captionate = false;
    var capTrack = 0;
} // End of Class
