class com.jeroenwijering.players.MP3Model extends com.jeroenwijering.players.AbstractModel
{
    var soundClip, positionInterval, feeder, currentItem, currentURL, soundObject, currentVolume, sendUpdate, loadedInterval, currentPosition, currentState;
    function MP3Model(vws, ctr, cfg, fed, scl)
    {
        super(vws, ctr, cfg, fed);
        soundClip = scl;
    } // End of the function
    function setStart(pos)
    {
        if (pos < 1)
        {
            pos = 0;
        }
        else if (pos > soundDuration - 1)
        {
            pos = soundDuration - 1;
        } // end else if
        clearInterval(positionInterval);
        if (feeder.feed[currentItem].file != currentURL)
        {
            var ref = this;
            currentURL = feeder.feed[currentItem].file;
            soundObject = new Sound(soundClip);
            soundObject.onSoundComplete = function ()
            {
                ref.currentState = 3;
                ref.sendUpdate("state", 3);
                ref.sendCompleteEvent();
            };
            soundObject.onLoad = function (scs)
            {
                if (scs == false)
                {
                    ref.currentState = 3;
                    ref.sendUpdate("state", 3);
                    ref.sendCompleteEvent();
                } // end if
            };
            soundObject.loadSound(currentURL, true);
            soundObject.setVolume(currentVolume);
            this.sendUpdate("load", 0);
            loadedInterval = setInterval(this, "updateLoaded", 200);
        } // end if
        if (pos != undefined)
        {
            currentPosition = pos;
            pos == 0 ? (this.sendUpdate("time", 0, soundDuration)) : (null);
        } // end if
        soundObject.start(currentPosition);
        this.updatePosition();
        this.sendUpdate("size", 0, 0);
        positionInterval = setInterval(this, "updatePosition", 200);
    } // End of the function
    function updateLoaded()
    {
        var _loc2 = Math.round(soundObject.getBytesLoaded() / soundObject.getBytesTotal() * 100);
        if (isNaN(_loc2))
        {
            currentLoaded = 0;
            this.sendUpdate("load", 0);
        }
        else if (_loc2 != currentLoaded)
        {
            this.sendUpdate("load", _loc2);
            currentLoaded = _loc2;
        }
        else if (_loc2 >= 100)
        {
            clearInterval(loadedInterval);
            currentLoaded = 100;
            this.sendUpdate("load", 100);
        } // end else if
    } // End of the function
    function updatePosition()
    {
        var _loc2 = soundObject.position / 1000;
        soundDuration = soundObject.duration / (10 * currentLoaded);
        if (_loc2 == currentPosition && currentState != 1)
        {
            currentState = 1;
            this.sendUpdate("state", 1);
        }
        else if (_loc2 != currentPosition && currentState != 2)
        {
            currentState = 2;
            this.sendUpdate("state", 2);
        } // end else if
        if (_loc2 != currentPosition)
        {
            currentPosition = _loc2;
            this.sendUpdate("time", currentPosition, soundDuration - currentPosition);
        } // end if
    } // End of the function
    function setPause(pos)
    {
        if (pos < 1)
        {
            pos = 0;
        }
        else if (pos > soundDuration - 1)
        {
            pos = soundDuration - 1;
        } // end else if
        soundObject.stop();
        clearInterval(positionInterval);
        currentState = 0;
        this.sendUpdate("state", 0);
        if (pos != undefined)
        {
            currentPosition = pos;
            this.sendUpdate("time", currentPosition, soundDuration - currentPosition);
        } // end if
    } // End of the function
    function setStop()
    {
        soundObject.stop();
        clearInterval(positionInterval);
        clearInterval(loadedInterval);
        delete this.currentURL;
        delete this.soundObject;
        soundDuration = 0;
        currentLoaded = 0;
    } // End of the function
    function setVolume(vol)
    {
        super.setVolume(vol);
        currentVolume = vol;
        soundObject.setVolume(vol);
    } // End of the function
    var mediatypes = new Array("mp3", "rbs");
    var currentLoaded = 0;
    var soundDuration = 0;
} // End of Class
