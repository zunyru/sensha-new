﻿class com.jeroenwijering.feeds.FeedManager
{
    var enclosures, stream, listeners, feed, feedXML, firstChild;
    function FeedManager(enc, jvs, pre, str)
    {
        enc == true ? (enclosures = true) : (enclosures = false);
        jvs == "true" ? (this.enableJavascript()) : (null);
        pre == undefined ? (null) : (prefix = pre);
        str == undefined ? (null) : (stream = "_" + str);
        listeners = new Array();
    } // End of the function
    function enableJavascript()
    {
        if (flash.external.ExternalInterface.available)
        {
            flash.external.ExternalInterface.addCallback("loadFile", this, loadFile);
            flash.external.ExternalInterface.addCallback("addItem", this, addItem);
            flash.external.ExternalInterface.addCallback("removeItem", this, removeItem);
            flash.external.ExternalInterface.addCallback("itemData", this, itemData);
        } // end if
    } // End of the function
    function loadFile(obj)
    {
        feed = new Array();
        for (var _loc7 in elements)
        {
            if (obj[_loc7] != undefined && obj[_loc7].indexOf("asfunction") == -1)
            {
                _root[_loc7] = obj[_loc7];
            } // end if
        } // end of for...in
        var _loc5 = "xml";
        var _loc3 = filetypes.length;
        while (--_loc3 >= 0)
        {
            if (obj.file.substr(0, 4).toLowerCase() == "rtmp")
            {
                _loc5 = "rtmp";
                continue;
            } // end if
            if (_root.type == filetypes[_loc3] || obj.file.substr(-3).toLowerCase() == filetypes[_loc3])
            {
                _loc5 = filetypes[_loc3];
            } // end if
        } // end while
        if (_loc5 == "xml" && obj.file.indexOf("asfunction") == -1)
        {
            this.loadXML(unescape(obj.file));
        }
        else
        {
            feed[0] = new Object();
            feed[0].type = _loc5;
            for (var _loc6 in elements)
            {
                if (_root[_loc6] != undefined)
                {
                    feed[0][_loc6] = unescape(_root[_loc6]);
                } // end if
            } // end of for...in
            this.playersPostProcess();
        } // end else if
    } // End of the function
    function loadXML(url)
    {
        var ref = this;
        feedXML = new XML();
        feedXML.ignoreWhite = true;
        feedXML.onLoad = function (scs)
        {
            if (scs)
            {
                var _loc3 = firstChild.nodeName.toLowerCase();
                if (_loc3 == "rss")
                {
                    ref.parser = new com.jeroenwijering.feeds.RSSParser(ref.prefix);
                    ref.feed = ref.parser.parse(this);
                }
                else if (_loc3 == "feed")
                {
                    ref.parser = new com.jeroenwijering.feeds.ATOMParser(ref.prefix);
                    ref.feed = ref.parser.parse(this);
                }
                else if (_loc3 == "playlist")
                {
                    ref.parser = new com.jeroenwijering.feeds.XSPFParser(ref.prefix);
                    ref.feed = ref.parser.parse(this);
                } // end else if
                if (_root.audio != undefined)
                {
                    ref.feed[0].audio = unescape(_root.audio);
                } // end if
                ref.playersPostProcess(url);
            } // end if
        };
        if (_root._url.indexOf("file://") > -1)
        {
            feedXML.load(url);
        }
        else if (url.indexOf("?") > -1)
        {
            feedXML.load(url + "&" + random(999));
        }
        else
        {
            feedXML.load(url + "?" + random(999));
        } // end else if
    } // End of the function
    function playersPostProcess(url)
    {
        url == undefined ? (null) : (this.filterOverlays());
        onlymp3s = true;
        feed.length > 1 ? (ischapters = true) : (ischapters = false);
        captions = false;
        audio = false;
        numads = 0;
        for (var _loc2 = 0; _loc2 < feed.length; ++_loc2)
        {
            if (enclosures == true && feed[_loc2].type == undefined && url != undefined)
            {
                feed[_loc2].type = "mp3";
                feed[_loc2].file = talkrURL + "?feed_url=" + url + "&permalink=" + feed[_loc2].link;
            }
            else if (stream == undefined)
            {
                feed[_loc2].file = prefix + feed[_loc2].file;
            }
            else if (feed[_loc2].type == "rtmp")
            {
                feed[_loc2].id = feed[_loc2].id + stream;
                feed[_loc2].file = prefix + feed[_loc2].file;
            }
            else if (feed[_loc2].type == "flv")
            {
                feed[_loc2].file = prefix + feed[_loc2].file.substr(0, feed[_loc2].file.length - 4) + stream + feed[_loc2].file.substr(-4);
            } // end else if
            if (feed[_loc2].type != "mp3")
            {
                onlymp3s = false;
            } // end if
            if (feed[_loc2].start == undefined)
            {
                feed[_loc2].start = 0;
            } // end if
            if (feed[_loc2].file != feed[0].file)
            {
                ischapters = false;
            } // end if
            if (feed[_loc2].captions != undefined)
            {
                captions = true;
            } // end if
            if (feed[_loc2].audio != undefined)
            {
                audio = true;
            } // end if
            if (feed[_loc2].category == "preroll" || feed[_loc2].category == "postroll")
            {
                ++numads;
                if (feed[_loc2].category == "preroll")
                {
                    feed[_loc2].image = feed[_loc2 + 1].image;
                } // end if
            } // end if
        } // end of for
        this.updateListeners();
    } // End of the function
    function filterOverlays()
    {
        for (var _loc2 = 0; _loc2 < feed.length; ++_loc2)
        {
            if (feed[_loc2].category == "overlay")
            {
                feed[_loc2 + 1].overlayfile = feed[_loc2].file;
                feed[_loc2 + 1].overlaylink = feed[_loc2].link;
                feed.splice(_loc2, 1);
                overlays = true;
            } // end if
        } // end of for
    } // End of the function
    function addItem(obj, idx)
    {
        if (obj.title == undefined)
        {
            obj.title = obj.file;
        } // end if
        if (obj.type == undefined)
        {
            obj.type = obj.file.substr(-3);
        } // end if
        if (arguments.length == 1 || idx >= feed.length)
        {
            feed.push(obj);
        }
        else
        {
            var _loc4 = feed.slice(0, idx);
            var _loc6 = feed.slice(idx);
            _loc4.push(obj);
            feed = _loc4.concat(_loc6);
        } // end else if
        this.updateListeners();
    } // End of the function
    function removeItem(idx)
    {
        if (feed.length == 1)
        {
            return;
        }
        else if (arguments.length == 0 || idx >= feed.length)
        {
            feed.pop();
        }
        else
        {
            feed.splice(idx, 1);
        } // end else if
        this.updateListeners();
    } // End of the function
    function itemData(idx)
    {
        return (feed[idx]);
    } // End of the function
    function addListener(lst)
    {
        listeners.push(lst);
    } // End of the function
    function removeListener(lst)
    {
        var _loc2 = listeners.length;
        while (--_loc2 >= 0)
        {
            if (listeners[_loc2] == lst)
            {
                listeners.splice(_loc2, 1);
                return;
            } // end if
        } // end while
    } // End of the function
    function updateListeners()
    {
        var _loc2 = listeners.length;
        while (--_loc2 >= 0)
        {
            listeners[_loc2].onFeedUpdate();
        } // end while
    } // End of the function
    var captions = false;
    var audio = false;
    var onlymp3s = false;
    var ischapters = true;
    var numads = 0;
    var overlays = false;
    var prefix = "";
    var talkrURL = "http://www.talkr.com/app/get_mp3.app";
    var filetypes = Array("flv", "mp3", "rbs", "jpg", "gif", "png", "rtmp", "swf", "mp4", "m4v", "m4a", "mov", "3gp", "3g2");
    var elements = {file: "", title: "บลาบลา", link: "", id: "", image: "", author: "", captions: "", audio: "", category: "", start: "", type: ""};
} // End of Class
