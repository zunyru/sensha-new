class com.jeroenwijering.players.JavascriptView extends com.jeroenwijering.players.AbstractView
{
    var sendEvent, loads, config, elaps, remain;
    function JavascriptView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        if (flash.external.ExternalInterface.available)
        {
            flash.external.ExternalInterface.addCallback("sendEvent", this, sendEvent);
        } // end if
    } // End of the function
    function getUpdate(typ, pr1, pr2)
    {
        if (flash.external.ExternalInterface.available)
        {
            switch (typ)
            {
                case "load":
                {
                    if (Math.round(pr1) != loads)
                    {
                        loads = Math.round(pr1);
                        flash.external.ExternalInterface.call("getUpdate", typ, loads, pr2, config.javascriptid);
                    } // end if
                    break;
                } 
                case "time":
                {
                    if (Math.round(pr1) != elaps || Math.round(pr2) != remain)
                    {
                        elaps = Math.round(pr1);
                        remain = Math.round(pr2);
                        flash.external.ExternalInterface.call("getUpdate", typ, elaps, remain, config.javascriptid);
                    } // end if
                    break;
                } 
                case "item":
                {
                    flash.external.ExternalInterface.call("getUpdate", typ, pr1, pr2, config.javascriptid);
                    break;
                } 
                default:
                {
                    flash.external.ExternalInterface.call("getUpdate", typ, pr1, pr2, config.javascriptid);
                    break;
                } 
            } // End of switch
        } // end if
    } // End of the function
} // End of Class
