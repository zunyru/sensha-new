class com.jeroenwijering.utils.Scroller
{
    var targetClip, maskClip, sizeRatio, scrollInterval, SCROLLER_CLIP, SCROLLER_FRONT_COLOR, _ymouse, _parent, _height, startDrag, stopDrag;
    function Scroller(tgt, msk, asc, fcl, hcl)
    {
        targetClip = tgt;
        maskClip = msk;
        arguments.length > 2 ? (autoScroll = asc) : (null);
        arguments.length > 3 ? (frontColor = fcl) : (null);
        arguments.length > 4 ? (lightColor = hcl) : (null);
        sizeRatio = maskClip._height / targetClip._height;
        if (autoScroll == false)
        {
            this.drawScrollbar();
        }
        else
        {
            scrollInterval = setInterval(this, "doAutoscroll", 50);
        } // end else if
        if (System.capabilities.os.toLowerCase().indexOf("mac") == -1)
        {
            Mouse.addListener(this);
        } // end if
    } // End of the function
    function drawScrollbar()
    {
        targetClip._parent.createEmptyMovieClip("scrollbar", targetClip._parent.getNextHighestDepth());
        SCROLLER_CLIP = targetClip._parent.scrollbar;
        SCROLLER_CLIP._x = maskClip._x + maskClip._width - 1;
        SCROLLER_CLIP._y = maskClip._y + 3;
        SCROLLER_CLIP.createEmptyMovieClip("back", 0);
        SCROLLER_CLIP.back._alpha = 0;
        SCROLLER_CLIP.back._y = -3;
        com.jeroenwijering.utils.Draw.square(SCROLLER_CLIP.back, 12, maskClip._height, frontColor);
        SCROLLER_CLIP.createEmptyMovieClip("bar", 1);
        SCROLLER_CLIP.bar._x = 4;
        SCROLLER_CLIP.bar._alpha = 50;
        com.jeroenwijering.utils.Draw.square(SCROLLER_CLIP.bar, 4, maskClip._height - 5, frontColor);
        SCROLLER_CLIP.createEmptyMovieClip("front", 2);
        SCROLLER_CLIP.front._x = 3;
        com.jeroenwijering.utils.Draw.square(SCROLLER_CLIP.front, 6, SCROLLER_CLIP.bar._height * sizeRatio, frontColor);
        SCROLLER_CLIP.front.createEmptyMovieClip("bg", 1);
        SCROLLER_CLIP.front.bg._x = -3;
        SCROLLER_CLIP.front.bg._alpha = 0;
        com.jeroenwijering.utils.Draw.square(SCROLLER_CLIP.front.bg, 12, SCROLLER_CLIP.front._height, frontColor);
        SCROLLER_FRONT_COLOR = new Color(SCROLLER_CLIP.front);
        this.setScrollbarEvents();
    } // End of the function
    function onMouseWheel(dta)
    {
        this.scrollTo(currentScroll - dta * 20);
    } // End of the function
    function doAutoscroll()
    {
        if (maskClip._xmouse > 0 && maskClip._xmouse < maskClip._width / (maskClip._xscale / 100) && maskClip._ymouse > 0 && maskClip._ymouse < maskClip._height / (maskClip._yscale / 100))
        {
            var _loc2 = maskClip._ymouse * (maskClip._yscale / 100) - maskClip._height / 2;
            this.scrollTo(currentScroll + Math.floor(_loc2 * AUTOSCROLL_SPEED));
        } // end if
    } // End of the function
    function setScrollbarEvents()
    {
        var instance = this;
        SCROLLER_CLIP.front.onRollOver = SCROLLER_CLIP.back.onRollOver = function ()
        {
            instance.SCROLLER_FRONT_COLOR.setRGB(instance.lightColor);
        };
        SCROLLER_CLIP.front.onRollOut = SCROLLER_CLIP.back.onRollOut = function ()
        {
            instance.SCROLLER_FRONT_COLOR.setRGB(instance.frontColor);
        };
        SCROLLER_CLIP.back.onPress = function ()
        {
            if (_ymouse > _parent.front._y + _parent.front._height)
            {
                instance.scrollTo(instance.currentScroll + instance.maskClip._height / 2);
            }
            else if (_ymouse < _parent.front._y)
            {
                instance.scrollTo(instance.currentScroll - instance.maskClip._height / 2);
            } // end else if
        };
        SCROLLER_CLIP.front.onPress = function ()
        {
            this.startDrag(false, 3, 0, 3, instance.SCROLLER_CLIP.bar._height - _height);
            instance.scrollInterval = setInterval(instance, "scrollTo", 100);
        };
        SCROLLER_CLIP.front.onRelease = SCROLLER_CLIP.front.onReleaseOutside = function ()
        {
            this.stopDrag();
            clearInterval(instance.scrollInterval);
        };
        this.scrollTo(maskClip._y - targetClip._y);
    } // End of the function
    function scrollTo(yps)
    {
        if (arguments.length == 0 && autoScroll == false)
        {
            yps = SCROLLER_CLIP.front._y * maskClip._height / SCROLLER_CLIP.front._height;
        } // end if
        if (yps < 5)
        {
            yps = 0;
        }
        else if (yps > targetClip._height - maskClip._height - 5)
        {
            yps = targetClip._height - maskClip._height;
        } // end else if
        com.jeroenwijering.utils.Animations.easeTo(targetClip, targetClip._x, maskClip._y - yps);
        SCROLLER_CLIP.front._y = yps * SCROLLER_CLIP.front._height / maskClip._height;
        currentScroll = yps;
    } // End of the function
    function purgeScrollbar()
    {
        clearInterval(scrollInterval);
        Mouse.removeListener(this);
        this.scrollTo(0);
        SCROLLER_CLIP.removeMovieClip();
    } // End of the function
    var autoScroll = false;
    var frontColor = 0;
    var lightColor = 0;
    var currentScroll = 0;
    var AUTOSCROLL_SPEED = 5.000000E-001;
} // End of Class
