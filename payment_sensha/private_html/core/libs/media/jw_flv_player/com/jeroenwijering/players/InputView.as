class com.jeroenwijering.players.InputView extends com.jeroenwijering.players.AbstractView
{
    var currentTime, currentVolume, sendEvent, feeder;
    function InputView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        Key.addListener(this);
    } // End of the function
    function setTime(elp, rem)
    {
        currentTime = elp;
    } // End of the function
    function setVolume(vol)
    {
        currentVolume = vol;
    } // End of the function
    function onKeyDown()
    {
        if (Key.getCode() == 32)
        {
            this.sendEvent("playpause");
        }
        else if (Key.getCode() == 37)
        {
            if (feeder.feed.length == 1)
            {
                this.sendEvent("scrub", currentTime - 15);
            }
            else
            {
                this.sendEvent("prev");
            } // end else if
        }
        else if (Key.getCode() == 39)
        {
            if (feeder.feed.length == 1)
            {
                this.sendEvent("scrub", currentTime + 15);
            }
            else
            {
                this.sendEvent("next");
            } // end else if
        }
        else if (Key.getCode() == 38)
        {
            this.sendEvent("volume", currentVolume + 10);
        }
        else if (Key.getCode() == 40)
        {
            this.sendEvent("volume", currentVolume - 10);
        }
        else if (Key.getCode() == 77)
        {
            this.sendEvent("volume", 0);
        } // end else if
    } // End of the function
} // End of Class
