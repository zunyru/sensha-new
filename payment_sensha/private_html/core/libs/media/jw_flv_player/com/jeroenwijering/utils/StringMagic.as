class com.jeroenwijering.utils.StringMagic
{
    function StringMagic()
    {
    } // End of the function
    static function stripTagsBreaks(str)
    {
        if (str.length == 0 || str == undefined)
        {
            return ("");
        } // end if
        var _loc4 = str.split("\n");
        str = _loc4.join("");
        _loc4 = str.split("\r");
        str = _loc4.join("");
        for (var _loc2 = str.indexOf("<"); _loc2 != -1; _loc2 = str.indexOf("<", _loc2))
        {
            var _loc3 = str.indexOf(">", _loc2 + 1);
            _loc3 == -1 ? (_loc3 = str.length - 1, str.length - 1) : (null);
            str = str.substr(0, _loc2) + str.substr(_loc3 + 1, str.length);
        } // end of for
        return (str);
    } // End of the function
    static function chopString(str, cap, nbr)
    {
        for (var _loc2 = cap; _loc2 < str.length; _loc2 = _loc2 + cap)
        {
            if (_loc2 == cap * nbr)
            {
                if (str.indexOf(" ", _loc2 - 5) == -1)
                {
                    return (str);
                }
                else
                {
                    return (str.substr(0, str.indexOf(" ", _loc2 - 5)));
                } // end else if
                continue;
            } // end if
            if (str.indexOf(" ", _loc2) > 0)
            {
                str = str.substr(0, str.indexOf(" ", _loc2 - 3)) + "\n" + str.substr(str.indexOf(" ", _loc2 - 3) + 1);
            } // end if
        } // end of for
        return (str);
    } // End of the function
    static function addLeading(nbr)
    {
        if (nbr < 10)
        {
            return ("0" + Math.floor(nbr));
        }
        else
        {
            return (Math.floor(nbr).toString());
        } // end else if
    } // End of the function
} // End of Class
