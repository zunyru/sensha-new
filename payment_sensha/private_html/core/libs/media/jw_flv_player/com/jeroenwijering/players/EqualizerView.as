class com.jeroenwijering.players.EqualizerView extends com.jeroenwijering.players.AbstractView
{
    var config, eqClip, eqStripes, currentVolume;
    function EqualizerView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        this.setupEQ();
        Stage.addListener(this);
    } // End of the function
    function setupEQ()
    {
        eqClip = config.clip.equalizer;
        eqClip._y = config.displayheight - 50;
        eqStripes = Math.floor((config.displaywidth - 20) / 6);
        eqClip.stripes.duplicateMovieClip("stripes2", 1);
        eqClip.mask.duplicateMovieClip("mask2", 3);
        eqClip.stripes._width = eqClip.stripes2._width = config.displaywidth - 20;
        eqClip.stripes.top.col = new Color(eqClip.stripes.top);
        eqClip.stripes.top.col.setRGB(config.lightcolor);
        eqClip.stripes.bottom.col = new Color(eqClip.stripes.bottom);
        eqClip.stripes.bottom.col.setRGB(16777215);
        eqClip.stripes2.top.col = new Color(eqClip.stripes2.top);
        eqClip.stripes2.top.col.setRGB(config.lightcolor);
        eqClip.stripes2.bottom.col = new Color(eqClip.stripes2.bottom);
        eqClip.stripes2.bottom.col.setRGB(16777215);
        eqClip.stripes.setMask(eqClip.mask);
        eqClip.stripes2.setMask(eqClip.mask2);
        eqClip.stripes._alpha = eqClip.stripes2._alpha = 50;
        setInterval(this, "drawEqualizer", 100, eqClip.mask);
        setInterval(this, "drawEqualizer", 100, eqClip.mask2);
    } // End of the function
    function drawEqualizer(tgt)
    {
        tgt.clear();
        tgt.beginFill(0, 100);
        tgt.moveTo(0, 0);
        var _loc5 = Math.round(currentVolume / 4);
        for (var _loc2 = 0; _loc2 < eqStripes; ++_loc2)
        {
            var _loc4 = random(_loc5) + _loc5 / 2 + 2;
            if (_loc2 == Math.floor(eqStripes / 2))
            {
                _loc4 = 0;
            } // end if
            tgt.lineTo(_loc2 * 6, -1);
            tgt.lineTo(_loc2 * 6, -_loc4);
            tgt.lineTo(_loc2 * 6 + 4, -_loc4);
            tgt.lineTo(_loc2 * 6 + 4, -1);
            tgt.lineTo(_loc2 * 6, -1);
        } // end of for
        tgt.lineTo(_loc2 * 6, 0);
        tgt.lineTo(0, 0);
        tgt.endFill();
    } // End of the function
    function setVolume(vol)
    {
        currentVolume = vol;
    } // End of the function
    function setState(stt)
    {
        stt == 2 ? (eqClip._visible = true) : (eqClip._visible = false);
    } // End of the function
    function onFullScreen(fs)
    {
        if (fs == true)
        {
            eqClip._visible = false;
        }
        else
        {
            eqClip._visible = true;
        } // end else if
    } // End of the function
} // End of Class
