<?php
class FlvPlayer{
	function player($video, $width, $height, $autoplay = 'false', $controls = 'true', $image){
		$player = '<script type="text/javascript" src="'.BASE_URL.'/libraries/media/jw_flv_player/swfobject.js"></script>';
		$player .='<p id="MyMovie"></p>';
		$player .='<script type="text/javascript">';
			$player .='var s1 = new SWFObject("'.BASE_URL.'/libraries/media/jw_flv_player/flvplayer.swf","single","'.$width.'","'.$height.'","7");';
			$player .='s1.addParam("allowfullscreen","true");';
			$player .='s1.addVariable("file","'.$video.'");';
			$player .='s1.addVariable("image","'.$image.'");';
			$player .='s1.addVariable("width","'.$width.'");';
			$player .='s1.addVariable("height","'.$height.'");';
            $player .='s1.addVariable("showdigits", "'.$controls.'");';
            $player .='s1.addVariable("autostart", "'.$autoplay.'");';
			$player .='s1.write("MyMovie");';
		$player .='</script>';
		
        return $player;
	}
	
	function youtube($video, $width = 425, $height = 344){
		$video = str_replace('watch?', '', $video);
		$video = str_replace('v=', 'v/', $video);
		$video = $video.'&hl=en&fs=1';
		$player = '<object width="'.$width.'" height="'.$height.'">';
		$player .= '<param name="movie" value="'.$video.'"</param>';
		$player .= '<param name="allowFullScreen" value="true"></param>';
		$player .= '<param name="allowscriptaccess" value="always"></param>';
		$player .= '<param name="wmode" value="transparent"></param>';
		$player .= '<embed src="'.$video.'" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" allowfullscreen="true" width="'.$width.'" height="'.$height.'"></embed>';
		$player .= '</object>'; 
		
		return $player;
	}	
}
?>