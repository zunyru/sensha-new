<?php
class CategoriesInfo {
	var $db;
	function CategoriesInfo(){
		$db = Database::getInstance();
		$this->db = $db;
		$this->split_tbl = array('video'); 
	}
	
	function load($field, $type, $condition = "" ){
		$tbl_ex = $type && in_array($type, $this->split_tbl) ? $type."_" : "";
		$sql = "
			SELECT ".$field."
			FROM ".DB_PREFIX.$tbl_ex."categories
			WHERE Active = '1' 
			".(SYS_Page == 2 ? "AND Publish = '1'" : "")."
			AND Type  = '".$type."'
			AND LangID = '".GetLangID()."'
			".$condition."
			LIMIT 1
		";   
		
		$rs = $this->db->GetArray( $sql );
		return $rs[0][$field];
	}
	
	function GetID($type, $condition = ''){
		return $this->load('CateID', $type, $condition);
	}
	
	function GetParentID($type, $condition = ''){
		return $this->load('CateID', $type, "AND Parent = '0'".$condition);
	}
	
	function GetParentTitle($type, $condition = ''){
		return $this->load('Title', $type, "AND Parent = '0'".$condition);
	}
	
	function GetTitle($type, $condition = ''){
		return $this->load('Title', $type, $condition);
	}
	
	function GetAlias($code, $condition = ''){
		return $this->load('Alias', $code, $condition);
	}
	
	function GetSubCategory($cateid, $link_subfix = '' ){ //Static function 
		$db = Database::getInstance();
		$this->db = $db;
		$sql = "
			SELECT CateID, CateTID, Parent, Title,  Type
			FROM ".DB_PREFIX."categories
			WHERE Active = '1' 
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND Parent = '".$cateid."'
			ORDER BY Title ASC
		";
		
		$rs = $this->db->GetArray( $sql );
		
		$base_url = SYS_PAGE == 1 ? BASE_URL_ADMIN : BASE_URL;
		
		$html = "";
		for($i = 0; $i < count($rs); $i++){
			$html .= '<a href="'.$base_url.'/index.php?app=categories&fnc=edit&cateid='.$rs[$i]['CateID'].$link_subfix.'">';
			$html .= $rs[$i]['Title'];
			$html .= '</a>';
			$html .= $i == count($rs) - 1 ? '' : ', ';
		}
		
		return $html;
	}
	
	function GetDropdown($type, $selected = '', $condition = '', $onchange_form = '', $first_label = ''){
		$html .= '<select name="cateid" id="cateid" class="txt_input"  '.($onchange_form ? $onchange_form : '').'>';
		if($first_label){
			$html .= '<option value="">'.$first_label.'</option>';
		}
		else{
			$html .= '<option value="">'._Please_Select.'</option>';
		}
		$html .= $this->GetRecuresiveOptions($type, '', $selected, $condition);
		$html .= '</select>';
		
		return $html;
	}
	
	function GetDropdownWithForm($type, $selected = '', $action = '', $condition = '', $onchange_form = '', $first_label = ''){
		$html .= '<script language="javascript">';
			$html .= 'function Cate2Go(){';
				$html .= 'document.forms[\'adminForm\'].action = \''.$action.'\';';
				$html .= 'document.forms[\'adminForm\'].submit();';
			$html .= '}';
		$html .= '</script>';
		$html .= '<select name="cateid" id="cateid" class="txt_input"  '.($onchange_form ? 'onChange="Cate2Go(); '.$onchange_form.'"' : 'onchange="Cate2Go();"').'>';
		if($params['first_label']){
			$html .= '<option value="">'.$first_label.'</option>';
		}
		else{
			$html .= '<option value="">'._Display_All.'</option>';
		}
		$html .= $this->GetRecuresiveOptions($type, '', $selected, $condition);
		$html .= '</select>';
		
		return $html;
	}
	
	function GetRecuresiveOptions($type, $cateid = '', $selected = '', $condition = '', $spacer = ''){
		$tbl_ex = $type && in_array($type, $this->split_tbl) ? $type."_" : "";
		$sql = "
			SELECT CateID, CateTID, Parent, Title,  Type
			FROM ".DB_PREFIX.$tbl_ex."categories
			WHERE Active = '1' 
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND Parent = '".($cateid ? $cateid : 0)."'
			AND LangID = '".GetLangID()."'
			".$condition."
			ORDER BY Title ASC
		";
	
		$rs = $this->db->GetArray( $sql );
		
		$base_url = SYS_PAGE == 1 ? BASE_URL_ADMIN : BASE_URL;
	
		$spacer .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		
		$html = "";
		for($i = 0; $i < count($rs); $i++){
			if($rs[$i]['Parent'] == 0){
				$spacer = "";
				$tree = "";
				$css_class = 'class="option_parent"';
			}
			else{
				$tree = "L&nbsp;";
				$css_class = 'class="option_child"';
			}
		
			$selected_abt = $rs[$i]['CateID'] == $selected ? 'selected="selected"' : '';
			
			$html .= '<option value="'.$rs[$i]['CateID'].'" '.$selected_abt.' '.$css_class.'>';
			$html .= $spacer.$tree.$rs[$i]['Title'];
			$html .= '</option>';
			$html .= $this->GetRecuresiveOptions($type, $rs[$i]['CateID'], $selected, $condition, $spacer);
			
		}
		return $html;
	 }
	
	function hasChild($cateid){
		$sql = "
			SELECT CateID
			FROM ".DB_PREFIX."categories
			WHERE Active = '1' 
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND Parent = '".($cateid ? $cateid : 0)."'
			AND LangID = '".GetLangID()."'
			AND CateID <> '".$cateid."'
			ORDER BY Title ASC
		";
		$rs = $this->db->Execute($sql);
		if($rs->RecordCount() > 0){
			return true;
		}
		else{
			return false;
		}
	}
}
?>