<?php
    class Model extends ADODB_Active_Record{
		var $db_prefix = DB_PREFIX;
		var $db;
		var $table_name;
		var $total_row;
		var $data_set;
		var $sql_command;
		
		function Model($table){
			$this->ADODB_Active_Record($this->getModelName($table));
		}	
		
		function getModelName($value){
			$value = strtolower($value);
			$value = str_replace('_model', '', $value);
			return $this->db_prefix.$value;
		}
		
		function save($id=''){
			if($id){
				$ok = $this->Update();
			}
			else{
				$ok = $this->Insert();
			}
			
			return $ok;
		}
		
		function set_query_limit($row_total, $per_page, $_page = '', $_limit = ''){
			$page = $_page ? $_page : param('page');
			$limit = $_limit ? $_limit : param('limit');
			
			if( $limit != 'unlimit' ){				
				if($page > 1 && $row_total > $per_page && (@(int)ceil($row_total/$per_page)) >= $page ){
					$sql = " LIMIT ".(($page-1) * $per_page).",".$per_page;	
				}
				else{
					$sql = " LIMIT 0,".$per_page;	
				}		
			}
			return $sql;
		}
		
		function get_total_rows($sql,$sql_param=array()){
			$rs_total = $this->db->Execute($sql,$sql_param);
			//echo "test";
			$row_total = $rs_total->RecordCount();
			return $row_total;
		}
		
		function get_total_row(){
			return $this->total_row;	
		}
		
		function get_table_name(){
			return $this->table_name;	
		}
		
		function get_sql_command(){
			return $this->sql_command;	
		}
		
		function get_data_set(){
			return $this->data_set;	
		}
	}
?>
