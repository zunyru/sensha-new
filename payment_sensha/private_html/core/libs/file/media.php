<?php
class Media{
	
	function upload($params){
		$db = Database::getInstance();	
		$group = $params['group'];
		$type = $params['type'];
		$source = $params['source'];
		$dest_path = $params['dest_path'];
		$resize = $params['resize'] ? true : false;
		$resize_x = $params['resize_x'];
		$resize_y = $params['resize_y'];
		$ratio_x = $params['ratio_x'] ? true : false;
		$ratio_y = $params['ratio_y'] ? true : false;
		$ref_id = $params['ref_id'];
		
		$o_media = new DBTable("media", "media_id");
		
//		if($ref_id){
//			$sql_media = "SELECT * FROM ".dbTable("media")." WHERE active = 1 AND ref_id = '".$ref_id."' ";
//			$rs_media = $db->GetArray($sql_media);
//			
//			$file_names = array();
//			for($i = 0; $i < count($rs_media); $i++){
//				$file_names[] = $rs_media[$i]['file_name'];	
//			}
//			$current_name = $media['file_name'];
//		}
		
		switch($type):
			case 'picture':
				require_once(BASE_PATH.'/libraries/file/upload/class.php');
				
				$handle = new Upload($source);
//				if( $current_name && is_file( $dest_path.'/'.$current_name ) ){
//					@unlink( $dest_path.'/'.$current_name );
//				}
				if ($handle->uploaded) {
			
					$handle->image_x = $resize_x;
					$handle->image_y = $resize_y;
					$handle->image_resize = $handle->image_src_x > $resize_x ? true : false;
					$handle->image_ratio_y 	= $resize;
					$handle->file_src_name_body = date("YmdHis");
					
					$handle->Process($dest_path.'/');
					
					if ($handle->processed) {
						$ds_media['lang_id'] 		= GetLangID();
						$ds_media['ref_id'] 			= $ref_id;
						$ds_media['group'] 			= $group;
						$ds_media['file_type']		= 'picture';
						$ds_media['file_name'] 	= $handle->file_dst_name;
						$ds_media['detail'] 			= $detail;
						$o_media->do_add($ds_media);
					}
				}
				$handle-> Clean();
				break;
			default:break;
		endswitch;
			
	}
	
}
?>