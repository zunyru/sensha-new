<?php
class File{
	protected static $instance = NULL;

	private function __construct(){}
	private function __clone(){}
	public function __destruct(){}

	public static function encode($file){
		if (!isset(self::$instance)){
			self::$instance = new File;
		}
		return self::$instance->base64_encode_file($file);
	}

	private function base64_encode_file($filename = string) {
		if ($filename && is_file($filename)) {
			$file_binary = fread(fopen($filename, "r"), filesize($filename));
			return 'data:' . File::getMimeType($filename) . ';base64,' . base64_encode($file_binary);
		}
	}

	private function getMimeType( $filename ) {
        $realpath = realpath( $filename );
        if ( $realpath
                && function_exists( 'finfo_file' )
                && function_exists( 'finfo_open' )
                && defined( 'FILEINFO_MIME_TYPE' )
        ) {
                // Use the Fileinfo PECL extension (PHP 5.3+)
                return finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $realpath );
        }
        if ( function_exists( 'mime_content_type' ) ) {
                // Deprecated in PHP 5.3
                return mime_content_type( $realpath );
        }
        return false;
	}
}
?>
