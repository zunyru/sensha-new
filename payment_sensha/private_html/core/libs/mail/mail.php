<?php
class SYSMail{
	function _($param){
		
		$body 			= $param['body'];
		$form_email 	= $param['from_email'] ? $param['from_email'] : $param['form_email'];
		$form_name 		= $param['from_name'] ? $param['from_name'] : $param['form_name'];
		$subject 		= $param['subject'];
		$email_to 		= $param['email_to'];
		$email_cc 		= $param['email_cc'];
		
		$arr_mail_to = array();
		$arr_mail_to = explode(',', $email_to);
		
		$arr_email_cc = array();
		$arr_email_cc = explode(',', $email_cc);
		
		if(count($param) >= 4){
			$mail = new phpmailer();
			$mail->CharSet		= MAIL_CHARSET;
			$mail->From     	= $form_email;			
			$mail->FromName 	= $form_name;			
			$mail->Subject 		= $subject;
			
			foreach($arr_mail_to as $mail_to){
				$mail->AddAddress( trim($mail_to) );
			}
			
			if( is_array($arr_mail_cc) && count($arr_mail_cc) > 0 ){
				foreach($arr_mail_cc as $mail_cc){
					$mail->AddCC( trim($mail_cc) );
				}
			}
			
			$mail->IsHTML(true);
			$mail->Body = $body;
			
			if(IS_SMTP && SMTP_HOST != "" && SMTP_USERNAME != "" && SMTP_PASSWORD != ""){ 
				$mail->IsSMTP(); // send via SMTP
				$mail->Host = SMTP_HOST; // SMTP servers
				$mail->SMTPAuth = true; // turn on SMTP authentication
				$mail->Username = SMTP_USERNAME; // SMTP username				
				$mail->Password = SMTP_PASSWORD; // SMTP password
				if(SMTP_PORT!='') {
					$mail->Port = SMTP_PORT;
				}
			}
			
			if($mail->Send()){
				$mail->ClearAddresses();
				return true;
			}			
			else{
				return false;	
			}
		}
		else{
			return false;
		}		
	}
}
?>