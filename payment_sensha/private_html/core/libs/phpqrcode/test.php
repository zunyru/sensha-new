<?php
ini_set('display_errors', 1);
ini_set('max_execution_time', (300*10)); //300 seconds = 5 minutes

include "qrlib.php";    

$link = mysqli_connect('localhost', 'root', 'password', 'emg');
mysqli_query($link, "SET NAMES UTF8");

$sql = "
    SELECT * FROM tmt_dealer_nostra WHERE dealer_nostra_head_id = 26 
        AND dealer_nostra_id IN(12034, 12035)
    ORDER BY dealer_id ASC
";
$result = mysqli_query($link, $sql);

while($row = mysqli_fetch_array($result)){
    $qrcode_file_name = $row['comp_code'].$row['branch_code'].'-'.str_replace(' ', '_', $row['name']).'_'.str_replace(array(' '), '_', $row['branch']).'.png';

    $url = 'http://www.toyotaemg.com/register/'.$row['comp_code'].$row['branch_code'];
    QRcode::png($url, 'EMG_QRCode/production_update/'.$qrcode_file_name, 'H', 20); // creates file 
    
    //echo $qrcode_file_name;
    echo $row['name']." ".$row['branch']." ".$row['comp_code']." ".$row['branch_code'];
    echo '<br>';
}

mysqli_close($link);
//QRcode::png('code data text', 'emg/filename.png', 'H', 20); // creates file 
?>