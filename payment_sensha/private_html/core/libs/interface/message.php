<?php
function kmsMessage( $msg_index = 'kms000' ){
	//Default
	$kmsMessage_set['kms000'] = "";
	// Save
	$kmsMessage_set['db100'] = kmsMessageStyle( 'false', _db100 );
	$kmsMessage_set['db101'] = kmsMessageStyle( 'true', _db101 );
	// Edit
	$kmsMessage_set['db200'] = kmsMessageStyle( 'false', _db200 );
	$kmsMessage_set['db201'] = kmsMessageStyle( 'true', _db201 );
	// Delete
	$kmsMessage_set['db300'] = kmsMessageStyle( 'false', _db300 );
	$kmsMessage_set['db301'] = kmsMessageStyle( 'orange', _db301 );
	
	return $kmsMessage_set[$msg_index];
}

function kmsMessageStyle( $type = '', $message ){
	if($type == "true"){
		$kmsMessage = "<label class='kmsMessage-true'>".$message."</label>";
	}
	elseif($type == "false"){
		$kmsMessage = "<label class='kmsMessage-false'>".$message."</label>";
	}
	elseif($type != ""){
		$kmsMessage = "<label style='color:".$type.";'>".$message."</label>";
	}
	else{
		$kmsMessage = "<label class='kmsMessage-default'>".$message."</label>";
	}
	
	return $kmsMessage;
}
?>