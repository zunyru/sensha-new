<?php
class Password{
	# The key (for security reason, it must be private)
	private $Key;
	// Generates the key
	public function __construct($Key = '') {
		$Key = str_split(md5($Key), 1);
		$signal = false;
		$sum = 0;
		foreach($Key as $char) {
			if($signal) {
				$sum -= ord($char);
				$signal = false;
			} else {
				$sum += ord($char);
				$signal = true;
			}
		}
		if($sum < 0){
			$sum *= -1;
		}

		$this->Key = $sum;
	}

	// Encrypt
	public function encrypt($text) {
		if($text != ""){
			$text = str_split($text, 1);
			$final = NULL;

			foreach($text as $char){
				$final .= sprintf("%03x", ord($char) + $this->Key);
			}

			$final = base64_encode($final);

			return $final;
		}
		else{
			return "";
		}
	}

	// Decrypt
	public function decrypt($text) {
		if($text != ""){
			$text = base64_decode($text);
			$final = NULL;
			$text = str_split($text, 3);

			foreach($text as $char){
				$final .= chr(hexdec($char) - $this->Key);
			}

			return $final;
		}
		else{
			return "";
		}
	}

	function generate($length = 8){
		$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$*';
		$str = '';
		$max = strlen($chars) - 1;
		for ($i=0; $i < $length; $i++){
			$str .= $chars[mt_rand(0, $max)];
		}
		return $str;
	}
}
?>
