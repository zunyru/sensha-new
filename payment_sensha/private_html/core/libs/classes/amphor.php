<?php
class Amphor_INI{
	function Amphor_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function options($provid, $id = ''){
		$sql = "
			SELECT AmpID, Title, Created, Publish
			FROM ".DB_PREFIX.'amphor'."
			WHERE Active = '1' 
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND LangID = '".GetLangID()."'
			".($provid ? "AND ProvID = '".$provid."'" : "")."
			ORDER BY Title ASC
		";
		$rs = $this->db->GetArray($sql);
		$options = "";
		for($i = 0; $i < count($rs); $i++){
			$selected = $id == $rs[$i]['AmpID'] ? 'selected="selected"' : '';
			$options .= '<option value="'.$rs[$i]['AmpID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $options;
	}
}
?>