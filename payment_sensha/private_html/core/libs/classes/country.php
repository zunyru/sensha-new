<?php
class Country_INI{
	function Country_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function options($id = ''){
		$sql = "
			SELECT CountryID, Title, Created, Publish
			FROM ".DB_PREFIX.'country'."
			WHERE Active = '1'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND LangID = '".GetLangID()."'
			ORDER BY Title ASC
		";
		$rs = $this->db->GetArray($sql);
		$options = "";
		for($i = 0; $i < count($rs); $i++){
			$selected = $id == $rs[$i]['CountryID'] ? 'selected="selected"' : '';
			$options .= '<option value="'.$rs[$i]['CountryID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $options;
	}
}
?>