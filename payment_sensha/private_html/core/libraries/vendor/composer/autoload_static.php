<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5c61b5b1a7fd0a6af5014afcc2fae52c
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'League\\Csv\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'League\\Csv\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/csv/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5c61b5b1a7fd0a6af5014afcc2fae52c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5c61b5b1a7fd0a6af5014afcc2fae52c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
