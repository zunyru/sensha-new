<?php
defined('WEB_ACCESS') or die('Access Denied.');

//date_default_timezone_set('Asia/Bangkok');

/**
 * Path & URL
 */
define('BASE_PATH', str_replace('/core/configs', '', str_replace('\\','/', realpath(dirname(__FILE__)))));
define('BASE_PRIVATE', BASE_PATH);
define('BASE_PUBLIC', str_replace((PRIVATE_DIR ? '/'.PRIVATE_DIR : '').'/core/configs', '', str_replace('\\','/', realpath(dirname(__FILE__)))).(PUBLIC_DIR ? '/'.PUBLIC_DIR : ''));
define('SITE_PATH', BASE_PRIVATE.'/'.(SITE_DIR ? SITE_DIR : ''));
define('SITE_PATH_PUBLIC', BASE_PUBLIC.'/'.(SITE_DIR ? SITE_DIR : ''));
//define('SITE_PATH_PUBLIC', BASE_PUBLIC.(SUB_DIR ? '/'.SUB_DIR : '').'/'.(SITE_DIR ? SITE_DIR : ''));

if( $_GET['lang'] && in_array($_GET['lang'], array('th', 'en')) ){
	SetLang($_GET['lang']);
}
else{
	//SetLang(DEFAULT_LANG);
	SetLang('th');
}

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	if(GetLang()=='en') {
		define('BASE_URL', 'https://'.$_SERVER['HTTP_HOST'].'/en'.SUB_DIR);
		define('BASE_SSL', 'https://'.$_SERVER['HTTP_HOST'].'/en'.SUB_DIR);
		define('MAIN_URL', 'https://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('PATH_LINK', BASE_PUBLIC.'/en');
	}
	else {
		define('BASE_URL', 'https://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('BASE_SSL', 'https://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('MAIN_URL', 'https://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('PATH_LINK', BASE_PUBLIC);
	}
}
else{
	if(GetLang()=='en') {
		define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].'/en'.SUB_DIR);
		define('BASE_SSL', 'https://'.$_SERVER['HTTP_HOST'].'/en'.SUB_DIR);
		define('MAIN_URL', 'http://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('PATH_LINK', BASE_PUBLIC.'/en');
	}
	else {	
		define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('BASE_SSL', 'https://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('MAIN_URL', 'http://'.$_SERVER['HTTP_HOST'].SUB_DIR);
		define('PATH_LINK', BASE_PUBLIC);
	}
}
define('SITE_URL', BASE_URL.'/'.(SITE_DIR ? SITE_DIR : ''));

define('BASE_BACKEND_PATH', BASE_PATH.'/backend');
define('BASE_BACKEND_URL', BASE_URL.'/backend');
define('BASE_BACKEND_SSL', BASE_SSL.'/backend');
define('BASE_FILE_PATH', BASE_PATH.'/files');
define('BASE_FILE_URL', BASE_URL.'/files');
define('BASE_TEMPLATE_URL', BASE_URL.'/template');

define('BASE_URL_ADMIN', BASE_BACKEND_URL);

if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
	define('BROWSER_VERSION', 'old');
}
else{
	define('BROWSER_VERSION', 'new');
}

/**
 * Site
 * WEB_SECRET_CODE : ใช้ชื่อโปรเจคผสมกับคำอื่น ๆ เพื่อนเข้ารหัสให้แตกต่างจากงานอื่น ๆ ใน core เดียวกัน
 * DETAULT_CHARSET : ใช้ระบุ charset ของ email หรือใช้กับ mb_string
 */
define('DETAULT_CHARSET', 'UTF-8');
define('MAIL_CHARSET', 'UTF-8');

/**
 * Default language
 */
define(WEB_SECRET_CODE.'DEFAULT_LANGUAGE', 'th');
define(WEB_SECRET_CODE.'DEFAULT_LANGUAGE_BACKEND', 'th');

/**
 * URL
 * URL_REWRITE :
 * 		0 = ใช้ url แบบปกติ : www.domain.com/index.php?app=content&cont_type=news&cat_code=sport&cat_id=2&page=5
 * 		1 = ใช้ url แบบ : www.domain.com/news/sport/page-5 (ต้องใช้ .htaccess เข้ามาช่วยในการ rewrite ด้วยสำหรับ linux)
 *		2 = ใช้ url แบบ : www.domain.com/index.php/app/content/cont_type/news/cat_code/sport/cat_id/2/page/5
 *			(กรณีที่ใช้ .htaccess ไม่ได้อย่างบน windows server ที่ไม่มี module rewrite url)
 * URL_EXTENSION : เพิ่มนามสกุลไฟล์ต่อท้าย url เช่น www.domain.com/news/sport/item001.html กรณีใช้ URL_REWRITE แบบ 1
 */
define('URL_REWRITE', 2);
define('URL_EXTENSION', '');

/**
 * Server setting
 */
ini_set('upload_max_filesize', '10M');
ini_set('memory_limit','128M');

if (!isset($_SERVER['REQUEST_URI'])) {
	$_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'],1 );
	if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != "") {
		$_SERVER['REQUEST_URI'] .= '?'.$_SERVER['QUERY_STRING'];
	}
}

/**
 * Error Setting
 */
//ini_set('display_errors', 1);
//error_reporting(E_ALL & ~(E_STRICT|E_NOTICE|E_DEPRECATED));
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_WARNING ^ E_STRICT ^ E_NOTICE ^ E_DEPRECATED);
?>
