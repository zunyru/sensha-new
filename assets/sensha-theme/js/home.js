// JavaScript Document
/*===================================
  slick
===================================*/

$(function() {
  $('.top-slide-bg').slick({
    pauseOnHover: false,
    pauseOnDotsHover: false,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 2300,
    infinite: true,
    arrows: false,
    fade: true,
    dots:true

  });
});


/*===================================
  tab change
===================================*/

$('.cl a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.form-content > div').not(target).hide();
  
  $(target).fadeIn(900);
  
});