<header id="header">
        <div class="header-wrap">
          <div class="header-main">
            <div class="inner">
              <a class="header-logo" href="index.php">
                <img src="images/logo.png" alt="SENSHA">
              </a>
              <ul class="header-menu">
                <li>
                  <a href="" data-src="#modal01" class="sign-up">
                    <span class="icon-person"></span>
                    Sign up
                  </a>
                </li>
                <li>
                  <a href="" data-src="#modal02" class="login">
                    <span class="icon-lock"></span>
                    Login
                  </a>
                </li>
                <li class="cart">
                  <a href="cart.php" class="">
                    <span class="icon-cart02"></span>
                    Cart
                  </a>
				  <div class="box-cart">
					   <h2><span class="icon-cart02"></span> Current Cart</h2>
					   <div class="clr">
					   <div class="box_domestic">
						   <div class="inner_box_domestic">
						   	    <span class="c01">Domestic</span>
							    <p class="items">5 items in cart</p>
							    <p class="sub">Subtotal 5,180 Yen</p>
								   <ul class="list-items">
									   <li>
										   <p>Import shipping</p>
										   <span>1,200 Yen</span>
									   </li>
									   <li>
										   <p>Import Tax</p>
										   <span>820  Yen</span>
									   </li>
									   <li>
										   <p>Minimum Shipping</p>
										   <span>900  Yen</span>
									   </li>
								   </ul>
						   </div>
						   <div class="total clr">
							   <p>Total</p>
							   <span>8,180 Yen</span>
						   </div>
					   </div>
					   <div class="box_oversea">
						   <div class="inner_box_domestic">
						   	    <span class="c01">Oversea</span>
							    <p class="items">5 items in cart</p>
							    <p class="sub">Subtotal 5,180 Yen</p>
								   <ul class="list-items">
									   <li>
										   <p>Import shipping</p>
										   <span>1,200 Yen</span>
									   </li>
									   <li>
										   <p>Import Tax</p>
										   <span>820  Yen</span>
									   </li>
									   <li>
										   <p>Minimum Shipping</p>
										   <span>900  Yen</span>
									   </li>
								   </ul>
						   </div>
						   <div class="total clr">
							   <p>Total</p>
							   <span>8,180 Yen</span>
						   </div>
					   </div>
					   </div>
					   <div class="line-btn"><input type="submit" class="buy btn" value="BUY NOW"></div>
				  </div>
                </li>
                <li class="lang">
                  <a href="" class="">
                    <span class="icon-lang"></span>
                    English
                  </a>
				  <div class="language-select">
					<ul>
					  <li><input type="radio" id="language01" name="language" value="English" data-label="language-select" data-value="English" checked="checked"><label for="language01">English</label></li>
					  <li><input type="radio" id="language02" name="language" value="Japanese" data-label="language-select" data-value="Japanese"><label for="language02">Japanese</label></li>
					  <li><input type="radio" id="language03" name="language" value="Chinese - Mandalin" data-label="language-select" data-value="Chinese - Mandalin"><label for="language03">Chinese - Mandalin</label></li>
					  <li><input type="radio" id="language04" name="language" value="Chinese - Cantonese" data-label="language-select" data-value="Chinese - Cantonese"><label for="language04">Chinese - Cantonese</label></li>
					  <li><input type="radio" id="language05" name="language" value="English - Us" data-label="language-select" data-value="English - Us"><label for="language05">English - Us</label></li>
					  <li><input type="radio" id="language06" name="language" value="Thai" data-label="language-select" data-value="Thai"><label for="language06">Thai</label></li>
					</ul>
				  </div>
                </li>
              </ul>
            </div>
          </div>
<!-- .header-main -->
          <div id="hamburger">
            <div id="hamburger-menu">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </div>
          </div>
          <div class="header-nav">
            <div class="inner">
              <nav>
                <ul>
                  <li>
                    <a href="">PPF</a>
                  </li>
                  <li>
                    <a href="product-category.php">Body</a>
                  </li>
                  <li>
                    <a href="">Window</a>
                  </li>
                  <li>
                    <a href="">Wheel / Tire</a>
                  </li>
                  <li>
                    <a href="">Mall / Bumper</a>
                  </li>
                  <li>
                    <a href="">Engine room</a>
                  </li>
                  <li>
                    <a href="">Interior</a>
                  </li>
                  <li>
                    <a href="">The Others</a>
                  </li>
                  <li class="lang sp_view">
                    <a href=""><span class="icon-lang"></span>English<span class="icon-arrow-bottom02"></span></a>
                  </li>
                </ul>
              </nav>
              <div class="select-list-block sp_view">
                <p class="ttl">PPF Search</p>
                <ul class="select-list">
                  <li class="maker">
                    <span class="icon-maker"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Maker">Maker</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                  <li class="model">
                    <span class="icon-model"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Model">Model</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                  <li class="part">
                    <span class="icon-part"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Part">Part</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                  <li class="year">
                    <span class="icon-year"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Year">Year</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                </ul>
                <div class="search-btn">
                  <button type="button">
                    <span>SEARCH</span>
                    <span class="icon-search02 ico-glass"></span>
                  </button>
                </div>
              </div>
              <div class="header-search">
                <a data-src="" class="search-button">PPF cut film
                  <span class="icon-search02 ico-glass"></span>
                </a>
				<div class="header-search-pop">
					<div class="search-content">
					  <p class="ttl">SEARCH</p>
					  <p class="lead">Easy to seach our product</p>
					  <ul class="search-list">
						<li class="maker">
						  <span class="icon-maker">Select Maker</span>
						  <form>
							<select name="Select Maker">
							  <option value="Select Maker">Select Maker</option>
							  <option value="bbbbbbbbbb">bbbbbbbbbb</option>
							  <option value="cccccccccc">cccccccccc</option>
							  <option value="dddddddddd">dddddddddd</option>
							</select>
						  </form>
						</li>
						<li class="model">
						  <span class="icon-model">Select Model</span>
						  <form>
							<select name="Select Maker">
							  <option value="Select Model">Select Model</option>
							  <option value="bbbbbbbbbb">bbbbbbbbbb</option>
							  <option value="cccccccccc">cccccccccc</option>
							  <option value="dddddddddd">dddddddddd</option>
							</select>
						  </form>
						</li>
						<li class="part">
						  <span class="icon-part">Select Part</span>
						  <form>
							<select name="Select Maker">
							  <option value="Select Part">Select Part</option>
							  <option value="bbbbbbbbbb">bbbbbbbbbb</option>
							  <option value="cccccccccc">cccccccccc</option>
							  <option value="dddddddddd">dddddddddd</option>
							</select>
						  </form>
						</li>
						<li class="year">
						  <span class="icon-year">Select Year</span>
						  <form>
							<select name="Select Maker">
							  <option value="Select Year">Select Year</option>
							  <option value="bbbbbbbbbb">bbbbbbbbbb</option>
							  <option value="cccccccccc">cccccccccc</option>
							  <option value="dddddddddd">dddddddddd</option>
							</select>
						  </form>
						</li>
					  </ul>
					  <div class="serch-btn">
						<button type="button" id="sbtn2">
						  <span>Search</span>
						  <span class="icon-search02 ico-glass"></span>
						</button>
					  </div>
					</div>
				</div>
              </div>
            </div>
          </div>
          <div class="sp-login">
            <a href="" data-src="#modal01" class="sp-login-btn">
              <span class="icon-person"></span>
              Login
            </a>
          </div>
        </div>
      </header>