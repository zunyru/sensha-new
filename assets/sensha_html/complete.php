<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><span>Payment complete</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
		    <div class="box-content">
				<div class="layout-contain">
					<div class="box-progress">
						<div class="progress-row">
							<div class="progress-step">
								<button type="button" class="btn-circle">1</button>
								<p>Cart item& Delivery</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle">2</button>
								<p>Shipping Address</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle">3</button>
								<p>Confirmation& Payment</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle step-complete">4</button>
								<p>Complete Order</p>
							</div> 
						</div>
					</div>
					<div class="topic">
						<p class="title-page">Payment complete</p>
					</div>
				  <div class="clr box_complete">
						<div class="left">
					    	<img src="images/img-complete.png">
						</div>
						<div class="right">
							<h2><img src="images/icon-check-g.png" style="width: 20px;margin-right:5px;"> You Order was successfully sent to SENSHA</h2>
							<p>You order successfully sent to SENSHA. Once SENSHA managed shipment,
we will you know via registered email.</p>
						  <p>For the payment reciept you can download from the order history in the below.
Also if you do not receive fuather shipment notification email or item delivered
even after 2 weeks. Please chaim to us from order history</p>
							<div style="text-align:left;margin-top:20px;">
							  <a href="order-history.php" class="b-blue"><img src="images/icon-check.png" style="width: 20px;margin-right: 5px;">Order history</a>
							</div>
						</div>
					</div>
				</div><!--layout-contain-->
			</div><!--box-content-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

